//
//  ModelLoader.swift
//  showary
//
//  Created by Michael Artuerhof on 14.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation
import SceneKit
import ARKit
import SSZipArchive

final class ModelLoader {
    init(transport: NetTransportService, disk: FileSaverService) {
        self.transport = transport
        self.disk = disk
    }

    // MARK: Private

    fileprivate struct const {
        static func extract(name: String) -> String {
            return (name as NSString).lastPathComponent
        }
    }

    fileprivate let disk: FileSaverService
    fileprivate let transport: NetTransportService
}

// MARK: - ModelLoaderService
extension ModelLoader: ModelLoaderService {
    fileprivate func modelFrom(url: URL, options: [String:String]? = nil) -> VirtualObject? {
        var scale: SCNVector3? = nil
        var alignment: ARRaycastQuery.TargetAlignment?
        if let opt = options {
            if let s = opt["scale"] {
                let ss = s.split(separator: ";").map({ String($0) })
                if ss.count > 2 {
                    scale = SCNVector3(ss[0].toDouble,
                                       ss[1].toDouble,
                                       ss[2].toDouble)
                }
            }
            if let a = opt[Const.ar.alignment] {
                switch a {
                case "horizontal":
                    alignment = .horizontal
                case "vertical":
                    alignment = .vertical
                default:
                    alignment = .any
                }
            }
        }
        if let node = VirtualObject(url: url) {
            node.load()
            let rootNode = node.childNodes.first
            if let s = scale {
                rootNode?.scale = s
            } else {
                rootNode?.scale = SCNVector3(0.05, 0.05, 0.05)
            }
            if let a = alignment {
                node.allowedAlignment = a
            }
            rootNode?.categoryBitMask = VirtualObjectType.obj.rawValue
            rootNode?.enumerateChildNodes { n, _ in
                n.categoryBitMask = VirtualObjectType.obj.rawValue
            }
            rootNode?.name = Const.root
            node.referenceURL = url
            return node
        }
        return nil
    }

    func loadFromNetworkModel(named: String,
                              options: [String:String]?,
                              completion: @escaping ((VirtualObject?, NSError?) -> Void)) {
        transport.getRawQuery(url: named, success: { [weak self] data in
            self?.disk.save(.model, name: const.extract(name: named), with: data)
            delay(1) { // wait till disk operations are over
                self?.loadModel(named: named,
                                options: options,
                                completion: completion)
            }
        }, failure: { e in
            completion(nil, e)
        })
    }

    fileprivate func extractedDirFrom(_ u: URL, modelName: String) -> URL {
        var extracted = u
        extracted.deleteLastPathComponent()
        extracted.appendPathComponent("extracted_\(modelName)")
        return extracted
    }

    func loadModel(named: String,
                   options: [String:String]?,
                   completion: @escaping ((VirtualObject?, NSError?) -> Void)) {
        func loadModelFrom(url: URL) {
            if let m = modelFrom(url: url, options: options) {
                completion(m, nil)
            } else {
                let e = NSError(domain: Const.domain,
                                code: -1,
                                userInfo: [
                                    NSLocalizedDescriptionKey:
                                        "Cannot load model from disk:".localized + named
                    ]
                )
                completion(nil, e)
            }
        }
        let modelName = const.extract(name: named)
        if disk.exists(type: .model, name: modelName) {
            let u = disk.urlFrom(type: .model, name: modelName)
            let extracted = extractedDirFrom(u, modelName: modelName)
            let fm = FileManager.default
            func firstFileInExtractedDir() -> URL {
                let path = extracted.path
                let d = try? fm.contentsOfDirectory(atPath: path)
                return extracted
                    .appendingPathComponent(d?.first ?? "")
            }
            if fm.fileExists(atPath: extracted.path) {
                loadModelFrom(url: firstFileInExtractedDir())
            } else {
                do {
                    try fm.createDirectory(at: extracted,
                                           withIntermediateDirectories: true,
                                           attributes: nil)
                }
                catch {
                    let e = NSError(domain: Const.domain,
                                    code: -1,
                                    userInfo: [
                                        NSLocalizedDescriptionKey:
                                        "loadModel: cannot create directory at path name: \"\(extracted.path)\""
                        ]
                    )
                    return completion(nil, e)
                }
                SSZipArchive.unzipFile(atPath: u.path,
                                       toDestination: extracted.path)
                delay(0.5) {
                    loadModelFrom(url: firstFileInExtractedDir())
                }
            }
        } else { // load model from network:
            loadFromNetworkModel(named: named, options: options, completion: completion)
        }
    }

    func deleteModel(named: String,
                     completion: @escaping (() -> Void)) {
        let modelName = const.extract(name: named)
        if disk.exists(type: .model, name: modelName) {
            let u = disk.urlFrom(type: .model, name: modelName)
            let extracted = extractedDirFrom(u, modelName: modelName)
            let fm = FileManager.default
            do {
                try fm.removeItem(at: u)
            }
            catch {
                print("deleteModel: Cannot delete '\(u.path)'; error: '\(error.localizedDescription)'")
            }
            try? fm.removeItem(at: extracted)
            delay(0.3) {
                completion()
            }
        } else {
            completion()
        }
    }
}
