//
//  ModelLoaderService.swift
//  showary
//
//  Created by Michael Artuerhof on 14.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation
import SceneKit

protocol ModelLoaderService {
    func loadModel(named: String,
                   options: [String:String]?,
                   completion: @escaping ((VirtualObject?, NSError?) -> Void))
    func deleteModel(named: String,
                     completion: @escaping (() -> Void))
}
