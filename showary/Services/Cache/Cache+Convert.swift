//
//  Cache+Convert.swift
//  showary
//
//  Created by Michael Artuerhof on 21/10/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

extension Cache: CacheConvertionService {
    func convert(object: Any) -> Any {
        if let ci = object as? CModel {
            return convert(model: ci)
        }
        fatalError("Cache: cannot find for core data object: \(object) an equivalent entity")
    }
}
