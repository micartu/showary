//
//  CModel+CoreDataProperties.swift
//  showary
//
//  Created by Michael Artuerhof on 08.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CModel> {
        return NSFetchRequest<CModel>(entityName: "CModel")
    }

    @NSManaged public var date: Date?
    @NSManaged public var descr: String?
    @NSManaged public var id: String?
    @NSManaged public var path: String?
    @NSManaged public var img: String?
    @NSManaged public var user: CUser?

}
