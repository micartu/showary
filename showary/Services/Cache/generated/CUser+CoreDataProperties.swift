//
//  CUser+CoreDataProperties.swift
//  showary
//
//  Created by Michael Artuerhof on 08.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CUser> {
        return NSFetchRequest<CUser>(entityName: "CUser")
    }

    @NSManaged public var email: String?
    @NSManaged public var login: String?
    @NSManaged public var name: String?
    @NSManaged public var surname: String?
    @NSManaged public var models: NSSet?

}

// MARK: Generated accessors for models
extension CUser {

    @objc(addModelsObject:)
    @NSManaged public func addToModels(_ value: CModel)

    @objc(removeModelsObject:)
    @NSManaged public func removeFromModels(_ value: CModel)

    @objc(addModels:)
    @NSManaged public func addToModels(_ values: NSSet)

    @objc(removeModels:)
    @NSManaged public func removeFromModels(_ values: NSSet)

}
