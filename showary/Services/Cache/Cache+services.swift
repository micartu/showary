//
//  Cache+services.swift
//  showary
//
//  Created by Michael Artuerhof on 11.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

extension Cache: ServiceLocatorModulProtocol {
    func registerServices(serviceLocator s: ServicesAssembler) {
        s.registerSingleton(singletonInstance: self as CacheUserService)
        s.registerSingleton(singletonInstance: self as CacheModelService)
        s.registerSingleton(singletonInstance: self as CacheFetchRequestsService)
    }
}
