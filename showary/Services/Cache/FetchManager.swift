//
//  FetchManager.swift
//  showary
//
//  Created by Michael Artuerhof on 18/11/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

protocol FetchManagerDelegate: class {
    func beginBatchUpdate()
    func make(change: CellChangeType)
    func endBatchUpdate()
}

protocol FetchManagerProtocol {
    var delegate: FetchManagerDelegate? { set get }
    /// fetches all data from db
    func fetchData()
    /// returns converted from db object
    func objectAt(_ indexPath: IndexPath) -> Any
    /// retuns count of objects for the given section
    func objectsCountFor(section: Int) -> Int
}

final class FetchManager<Type: NSFetchRequestResult>: NSObject,
    NSFetchedResultsControllerDelegate {
    weak var delegate: FetchManagerDelegate? = nil
    fileprivate let fetchedResultsController: NSFetchedResultsController<Type>
    fileprivate let converter: CacheConvertionService

    init(fetchedResultsController: NSFetchedResultsController<Type>,
         converter: CacheConvertionService) {
        self.fetchedResultsController = fetchedResultsController
        self.converter = converter
        super.init()
        fetchedResultsController.delegate = self
    }

    // MARK: NSFetchedResultsControllerDelegate

    public func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.beginBatchUpdate()
    }

    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.endBatchUpdate()
    }

    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                           didChange anObject: Any,
                           at indexPath: IndexPath?,
                           for type: NSFetchedResultsChangeType,
                           newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let indexPath = newIndexPath else { fatalError("Index path should be not nil") }
            delegate?.make(change: .insert(index: indexPath))
        case .update:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            delegate?.make(change: .update(index: indexPath))
        case .move:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            guard let newIndexPath = newIndexPath else { fatalError("New index path should be not nil") }
            delegate?.make(change: .delete(index: indexPath))
            delegate?.make(change: .insert(index: newIndexPath))
        case .delete:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            delegate?.make(change: .delete(index: indexPath))
        @unknown default:
            fatalError("FetchManager: unknown NSFetchedResultsChangeType: \(type)")
        }
    }
}

// MARK: FetchManagerProtocol
extension FetchManager: FetchManagerProtocol {
    func fetchData() {
        // call fetch request for receiving all needed data from db
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("FetchManager: problems while fetching data from core data: \(error)")
        }
    }

    func objectAt(_ indexPath: IndexPath) -> Any {
        let obj = fetchedResultsController.object(at: indexPath)
        return converter.convert(object: obj)
    }

    func objectsCountFor(section: Int) -> Int {
        guard let section = fetchedResultsController
            .sections?[section] else { return 0 }
        return section.numberOfObjects
    }
}
