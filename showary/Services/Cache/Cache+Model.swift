//
//  Cache+Model.swift
//  showary
//
//  Created by Michael Artuerhof on 19.12.2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

extension Cache: CacheModelService {
    func modelExistsWith(id: String) -> Bool {
        return existsEntityWithPredicate(getModelPredicate(id: id),
                                         EntityType: CModel.self,
                                         in: getContext())
    }

    func create(models: [Model],
                completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            self.createOrUpdateModels(models, in: context)
        }, completion: completion)
    }

    func update(models: [Model], completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            for c in models {
                if let cc = self.getCModelWithID(c.id,
                                                 in: context) {
                    self.updateModelContents(cc, from: c)
                }
            }
        }, completion: completion)
    }

    func modelWith(id: String) -> Model? {
        if let ci = getCModelWithID(id,
                                    in: getContext()) {
            return convert(model: ci)
        }
        return nil
    }

    func modelsForCurrentUser() -> [Model] {
        var o = [Model]()
        let ctx = getContext()
        if let cu = getCurrentCUser(in: ctx) {
            if let itms = cu.models?
                .sortedArray(
                    using: [NSSortDescriptor(key: "date", ascending: false)]
                ) as? [CModel] {
                for ci in itms {
                    let i = convert(model: ci)
                    o.append(i)
                }
            }
        }
        return o
    }

    func delete(modelIDs: [ModelDescProtocol], completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            for i in modelIDs {
                self.deleteModelWithEAN(i.id, in: context)
            }
        }, completion: completion)
    }

    func deleteAllModelsForCurrentUser(completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            if let cu = self.getCurrentCUser(in: context) {
                if let cmnts = cu.models as? Set<CModel> {
                    for c in cmnts {
                        self.deleteCModel(c, in: context)
                    }
                }
            }
        }, completion: completion)
    }

    func deleteAllModels(completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            if let cmnts = CModel.mr_findAll(in: context) as? [CModel] {
                for c in cmnts {
                    self.deleteCModel(c, in: context)
                }
            }
        }, completion: completion)
    }

    // MARK: - Private/Internal

    internal func getModelPredicate(id: String) -> NSPredicate {
        let idPred = getIDStrPredicate(value: id)
        let usrLogin = getStrPredicate(key: "user.login", value: secret.login)
        return NSCompoundPredicate(andPredicateWithSubpredicates: [idPred, usrLogin])
    }

    internal func getCModelWithID(_ id: String, in context: NSManagedObjectContext) -> CModel? {
        return CModel.mr_findFirst(
            with: getModelPredicate(id: id),
            in: context)
    }

    internal func getCModelWithID(_ id: String, serial: String) -> CModel? {
        return getCModelWithID(id, in: getContext())
    }

    @discardableResult
    internal func createCModel(_ c: Model, in context: NSManagedObjectContext) -> CModel {
        let cc = CModel.mr_createEntity(in: context)!
        cc.date = Date()
        updateModelContents(cc, from: c)
        return cc
    }

    internal func createOrUpdateModels(_ models: [Model],
                                      in context: NSManagedObjectContext) {
        guard let cu = getCurrentCUser(in: context) else { return }
        for m in models {
            let ci: CModel
            if let _ci = getCModelWithID(m.id, in: context) {
                ci = _ci
                updateModelContents(ci, from: m)
            } else {
                ci = createCModel(m, in: context)
            }
            cu.addToModels(ci)
        }
    }

    internal func updateModelContents(_ cm: CModel, from m: Model) {
        cm.id = m.id
        cm.img = m.img
        cm.path = m.path
        cm.descr = m.descr
	}

    internal func convert(model c: CModel) -> Model {
        return Model(id: c.id!,
                     img: c.img ?? "",
                     path: c.path!,
                     descr: c.descr!)
    }

    internal func deleteCModel(_ c: CModel, in context: NSManagedObjectContext) {
        c.mr_deleteEntity(in: context)
    }

    internal func deleteModelWithEAN(_ id: String, in context: NSManagedObjectContext) {
        if let c = getCModelWithID(id, in: context) {
            deleteCModel(c, in: context)
        }
    }
}

// part of CacheFetchRequestsService
extension Cache {
    func fetchDataControllerForModelsForUser(login: String) -> FetchManagerProtocol {
        let context = NSManagedObjectContext.mr_default()
        let predicate = getStrPredicate(key: "user.login", value: login)
        let request = CModel.mr_requestAllSorted(by: "date",
                                                ascending: false,
                                                with: predicate,
                                                in: context)
        request.returnsObjectsAsFaults = false
        request.fetchBatchSize = const.szBatch
        let frc = NSFetchedResultsController(fetchRequest: request,
                                             managedObjectContext: context,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil) as! NSFetchedResultsController<CModel>
        return FetchManager(fetchedResultsController: frc,
                            converter: self)
    }
}
