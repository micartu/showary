//
//  Cache+User.swift
//  Chatter
//
//  Created by Michael Artuerhof on 09/07/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

extension Cache: CacheUserService {
    func userExistsWith(login: String) -> Bool {
        return exists(login: login, in: getContext())
    }

    func getUserWith(login: String) -> User? {
        if let u = getCUserOfLogin(login) {
            return convert(user: u)
        }
        return nil
    }

    func getOrGenerateCurrentUser(completion: @escaping ((User) -> Void)) {
        var usr: User?
        modify({ [unowned self] context in
            let u = self.getOrGenerateCurrentCUser(in: context)
            usr = self.convert(user: u)
        }, completion: {
            if let u = usr {
                completion(u)
            }
        })
    }

    func create(user: User, completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            self.create(user: user, in: context)
        }, completion: completion)
    }

    func update(user: User, completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            if let u = self.getCUserOfLogin(user.login, in: context) {
                self.update(user: u, from: user)
            } else {
                // was the current user generated?
                if let cu = self.getCurrentCUser(in: context),
                    self.secret.isUserGenerated {
                    // yes, update it with that one, authorized!
                    self.secret.isUserGenerated = false // reset generated hint
                    self.update(user: cu, from: user)
                } else { // nope, no loaded models, then create a new one:
                    self.create(user: user, in: context)
                }
            }
        }, completion: completion)
    }

    func deleteUserWith(login: String, completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            self.delete(login: login, in: context)
        }, completion: completion)
    }

    // MARK: - Internal
    @discardableResult
    internal func getOrGenerateCurrentCUser(in context: NSManagedObjectContext) -> CUser {
        if let u = getCurrentCUser(in: context) {
            return u
        } else {
            let id = UUID().uuidString
            let u = User(login: id,
                         name: id,
                         email: "",
                         password: "")
            secret.login = id
            secret.isUserGenerated = true
            return create(user: u, in: context)
        }
    }

    internal func getCurrentCUser(in context: NSManagedObjectContext) -> CUser? {
        let login = secret.login
        if let u = getCUserOfLogin(login, in: context) {
            return u
        }
        return nil
    }

    internal func getCUserOfLogin(_ login: String, in context: NSManagedObjectContext) -> CUser? {
        let predicate = getStrPredicate(key: "login", value: login)
        return CUser.mr_findFirst(with: predicate, in: context)
    }

    internal func getCUserOfLogin(_ login: String) -> CUser? {
        return getCUserOfLogin(login, in: getContext())
    }

    internal func convert(user cu: CUser) -> User {
        return User(login: cu.login!,
                    name: cu.name!,
                    email: cu.email!,
                    password: "")
    }

    @discardableResult
    internal func create(user: User, in context: NSManagedObjectContext) -> CUser {
        let u = CUser.mr_createEntity(in: context)!
        update(user: u, from: user)
        return u
    }

    internal func update(user cu: CUser, from u: User) {
        cu.login = u.login
        cu.name = u.name
        cu.email = u.email
    }

    internal func existsUserId(id: String, in context: NSManagedObjectContext) -> Bool {
        return existsEntityWith(id,
                                EntityType: CUser.self,
                                in: context)
    }

    internal func exists(login: String, in context: NSManagedObjectContext) -> Bool {
        return existsEntityWith(login,
                                EntityType: CUser.self,
                                in: context,
                                key: "login")
    }

    internal func deleteCUser(_ cu: CUser, in context: NSManagedObjectContext) {
        cu.mr_deleteEntity(in: context)
    }

    internal func delete(login: String, in context: NSManagedObjectContext) {
        let predicate = getStrPredicate(key: "login", value: login)
        if let users = CUser.mr_findAll(with: predicate, in: context) as? [CUser] {
            for u in users {
                deleteCUser(u, in: context)
            }
        }
    }
}
