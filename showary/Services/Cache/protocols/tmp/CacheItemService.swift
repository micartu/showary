//
//  CacheItemService.swift
//  showary
//
//  Created by Michael Artuerhof on 19.12.2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol ItemDescProtocol {
    var ean: String { get }
    var serial: String { get }
}

struct ItemDesc: ItemDescProtocol {
    let ean: String
    let serial: String
}

protocol CacheItemService {
    func itemExistsWith(barcode: String, serial: String) -> Bool
    func itemWith(barcode: String) -> Item?
    func itemWith(barcode: String, serial: String) -> Item?
    func create(items: [Item],
                completion: @escaping (() -> Void))
    func update(items: [Item],
                completion: @escaping (() -> Void))
    func itemsForCurrentUser() -> [Item]
    func delete(itemEANs: [ItemDescProtocol], completion: @escaping (() -> Void))
    func deleteAllItemsForCurrentUser(completion: @escaping (() -> Void))
    func deleteAllItems(completion: @escaping (() -> Void))
}
