//
//  CacheModelService.swift
//  showary
//
//  Created by Michael Artuerhof on 19.12.2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol ModelDescProtocol {
    var id: String { get }
}

struct ModelDesc: ModelDescProtocol {
    let id: String
}

protocol CacheModelService {
    func modelExistsWith(id: String) -> Bool
    func modelWith(id: String) -> Model?
    func create(models: [Model],
                completion: @escaping (() -> Void))
    func update(models: [Model],
                completion: @escaping (() -> Void))
    func modelsForCurrentUser() -> [Model]
    func delete(modelIDs: [ModelDescProtocol], completion: @escaping (() -> Void))
    func deleteAllModelsForCurrentUser(completion: @escaping (() -> Void))
    func deleteAllModels(completion: @escaping (() -> Void))
}
