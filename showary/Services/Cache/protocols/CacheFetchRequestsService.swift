//
//  CacheFetchRequestsService.swift
//  showary
//
//  Created by Michael Artuerhof on 22/10/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol CacheFetchRequestsService {
    func fetchDataControllerForModelsForUser(login: String) -> FetchManagerProtocol
}
