//
//  CacheUserService.swift
//  showary
//
//  Created by Michael Artuerhof on 19.12.2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol CacheUserService {
    func userExistsWith(login: String) -> Bool
    func getUserWith(login: String) -> User?
    func getOrGenerateCurrentUser(completion: @escaping ((User) -> Void))
    func create(user: User, completion: @escaping (() -> Void))
    func update(user: User, completion: @escaping (() -> Void))
    func deleteUserWith(login: String, completion: @escaping (() -> Void))
}
