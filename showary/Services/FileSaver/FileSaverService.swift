//
//  FileSaverService.swift
//  showary
//
//  Created by Michael Artuerhof on 13.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

enum FileTypes {
    case image
    case smallImage
    case model
}

protocol FileSaverService {
    func exists(type: FileTypes, name: String) -> Bool
    func urlFrom(type: FileTypes, name: String) -> URL
    func filesFor(type: FileTypes) -> [String]
    func dateFor(path: String) -> Date?
    @discardableResult
    func save(_ type: FileTypes, name: String, with data: Data) -> String
    func contentsOf(type: FileTypes, name: String) -> Data?
    func load(image: String) -> UIImage?
    func loadSmall(image: String) -> UIImage?
    func remove(type: FileTypes, name: String)
}
