//
//  KeyValueContainer.swift
//  showary
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

final class KeyValueContainer<T: Codable> {
    let storage: KeyValueStorageService
    let key: String
    let defaultValue: T?

    var value: T? {
        get {
            storage.value(forKey: key) ?? defaultValue
        }
        set {
            storage.setValue(newValue, forKey: key)
        }
    }

    init(storage: KeyValueStorageService,
         key: String,
         defaultValue: T? = nil) {
        self.storage = storage
        self.key = key
        self.defaultValue = defaultValue
    }
}
