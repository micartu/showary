//
//  ServicesAssembler.swift
//  showary
//
//  Created by Michael Artuerhof on 23.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol ServiceLocatorModulProtocol {
    func registerServices(serviceLocator s: ServicesAssembler)
}

final class ServicesAssembler {
    init() {
        let kv = KeyValueStorage(suiteName: nil)!
        registerSingleton(singletonInstance: kv as KeyValueStorageService)

        let secrets = Secret(storage: kv)
        registerSingleton(singletonInstance: secrets as SecretService)

        let tm = ThemeManager()
        registerSingleton(singletonInstance: tm as ThemeManagerProtocol)

        let cfgName: String
        #if DEBUG
        cfgName = "network.debug"
        #else
        cfgName = "network"
        #endif
        let cfg = NetworkConfig.getConfiguration(name: cfgName)
        let transport = MobileNetTransport(config: cfg, secret: secrets)
        let manager = NetManager(cfg: cfg,
                                 secret: secrets,
                                 transport: transport)
        manager.registerServices(serviceLocator: self)

        let cache = Cache(secret: secrets)
        cache.registerServices(serviceLocator: self)
        
        let snd = SoundBox()
        registerSingleton(singletonInstance: snd as SoundBoxService)

        let disk = FileSaver()
        registerSingleton(singletonInstance: disk as FileSaverService)

        let imageLoader = ImageLoader(transport: transport,
                                      disk: disk)
        registerSingleton(singletonInstance: imageLoader as ImageLoaderService)

        let modelLoader = ModelLoader(transport: transport, disk: disk)
        registerSingleton(singletonInstance: modelLoader as ModelLoaderService)
    }

    // MARK: Registration

    public func register<Service, T>(factory: @escaping (T) -> Service) {
        let serviceId = ObjectIdentifier(Service.self)
        registry[serviceId] = factory
    }

    public static func register<Service>(factory: @escaping () -> Service) {
        sharedServices.register(factory: factory)
    }

    public static func register<Service>(factory: @escaping (String) -> Service) {
        sharedServices.register(factory: factory)
    }

    public func registerSingleton<Service>(singletonInstance: Service) {
        let serviceId = ObjectIdentifier(Service.self)
        registry[serviceId] = singletonInstance
    }

    public static func registerSingleton<Service>(singletonInstance: Service) {
        sharedServices.registerSingleton(singletonInstance: singletonInstance)
    }

    public func registerModules(modules: [ServiceLocatorModulProtocol]) {
        modules.forEach { $0.registerServices(serviceLocator: self) }
    }

    public static func registerModules(modules: [ServiceLocatorModulProtocol]) {
        sharedServices.registerModules(modules: modules)
    }

    // MARK: Injection

    public static func inject<Service>() -> Service {
        return sharedServices.inject()
    }

    // MARK: private

    private func inject<Service>() -> Service {
        let serviceId = ObjectIdentifier(Service.self)
        if let factory = registry[serviceId] as? () -> Service {
            return factory()
        } else if let factory = registry[serviceId] as? (String) -> Service {
            let id = String(describing: Service.self)
            return factory(id)
        }
        else if let singletonInstance = registry[serviceId] as? Service {
            return singletonInstance
        } else {
            fatalError("No registered entry for \(Service.self)")
        }
    }

    private var registry = [ObjectIdentifier:Any]()
    private static var sharedServices: ServicesAssembler = {
        let shared = ServicesAssembler()
        return shared
    }()
}
