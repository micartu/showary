//
//  Secret.swift
//  showary
//
//  Created by Michael Artuerhof on 26.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

final class Secret: SecretService {



    private let storage: KeyValueStorageService

    init(storage: KeyValueStorageService) {
        self.storage = storage
    }

    func erase() {
        isAuthCreated = false
        session = ""
        restore = ""
        login = ""
        password = ""
        uid = ""
    }

    var authorized: Bool {
        get {
            return isAuthCreated
        }
    }

    var isAuthCreated: Bool {
        get {
            return storage.isAuthCreated.value ?? false
        }
        set {
            storage.isAuthCreated.value = newValue
        }
    }

    // MARK: session and stuff

    var session: String {
        get {
            return storage.session.value ?? ""
        }
        set {
            storage.session.value = newValue
        }
    }
    var restore: String {
        get {
            return storage.restore.value ?? ""
        }
        set {
            storage.restore.value = newValue
        }
    }
    var login: String {
        get {
            return storage.login.value ?? ""
        }
        set {
            storage.login.value = newValue
        }
    }
    var password: String {
        get {
            return storage.password.value ?? ""
        }
        set {
            storage.password.value = newValue
        }
    }
    var uid: String {
        get {
            return storage.uid.value ?? ""
        }
        set {
            storage.uid.value = newValue
        }
    }
    var isUserGenerated: Bool {
        get {
            return storage.isUserGenerated.value ?? false
        }
        set {
            storage.isUserGenerated.value = newValue
        }
    }


    // MARK: social networks stuff

    var isSocialAuth: Bool {
        get {
            return storage.isSocialAuth.value ?? false
        }
        set {
            storage.isSocialAuth.value = newValue
        }
    }

    var socialAuthType: String {
        get {
            return storage.socialAuthType.value ?? ""
        }
        set {
            storage.socialAuthType.value = newValue
        }
    }
    var socialAuthExtra: [String:String] {
        get {
            return storage.socialAuthExtra.value ?? [String:String]()
        }
        set {
            storage.socialAuthExtra.value = newValue
        }
    }
    var socialAuthToken: String {
        get {
            return storage.socialAuthToken.value ?? ""
        }
        set {
            storage.socialAuthToken.value = newValue
        }
    }
    var socialAuthExpire: Date? {
        get {
            return storage.socialAuthExpire.value ?? Date()
        }
        set {
            storage.socialAuthExpire.value = newValue
        }
    }
}

// MARK: - K-V-Contrainer definitions for needed fields
private extension KeyValueStorageService {
    var isAuthCreated: KeyValueContainer<Bool> {
        makeContainer()
    }
    var session: KeyValueContainer<String> {
        makeContainer()
    }
    var restore: KeyValueContainer<String> {
        makeContainer()
    }
    var login: KeyValueContainer<String> {
        makeContainer()
    }
    var password: KeyValueContainer<String> {
        makeContainer()
    }
    var uid: KeyValueContainer<String> {
        makeContainer(key: #function, def: "X-X-X")
    }
    var isUserGenerated: KeyValueContainer<Bool> {
           makeContainer()
    }

    // social

    var isSocialAuth: KeyValueContainer<Bool> {
        makeContainer()
    }
    var socialAuthType: KeyValueContainer<String> {
        makeContainer()
    }
    var socialAuthExtra: KeyValueContainer<[String:String]> {
        makeContainer()
    }
    var socialAuthToken: KeyValueContainer<String> {
        makeContainer()
    }
    var socialAuthExpire: KeyValueContainer<Date> {
        makeContainer()
    }
}
