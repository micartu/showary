//
//  SoundBox.swift
//  showary
//
//  Created by Michael Artuerhof on 22.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation
import AVFoundation

class SoundBox {
    private var player: AVAudioPlayer?
}

extension SoundBox: SoundBoxService {
    func play(sound: String) {
        guard let url = Bundle.main.url(
            forResource: sound,
            withExtension: "mp3"
            ) else { return }
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(.playback, mode: .default)
            try session.setActive(true)
            player = try AVAudioPlayer(
                contentsOf: url,
                fileTypeHint: AVFileType.mp3.rawValue
            )
            player?.play()
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
