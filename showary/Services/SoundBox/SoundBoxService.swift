//
//  SoundBoxService.swift
//  showary
//
//  Created by Michael Artuerhof on 22.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

protocol SoundBoxService {
    func play(sound: String)
}
