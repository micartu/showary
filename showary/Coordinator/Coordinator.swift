//
//  Coordinator.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol Coordinator {
    var viewControllable: UIViewController? { get }
    func start()
    func stop()
    func presented(on vc: UIViewController, what nvc: UIViewController?)
}

protocol ParentCoordinator: class {
    func exitFrom(coordinator: BaseCoordinator, with result: Any?)
}

protocol BaseDispatcher: class {
    func exitWith(result: Any?)
}

protocol CommonCoordinator: ParentCoordinator, BaseDispatcher, AlertProtocol { }
