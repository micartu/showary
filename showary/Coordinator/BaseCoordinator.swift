//
//  BaseCoordinator.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

class BaseCoordinator:
    CoordinatorProtocol,
    ParentCoordinatorProtocol,
    BaseDispatcherProtocol {
    /// is it busy now?
    internal let busy = Atomic<Bool>(false)

    /// controller - a wrapper around view controller
    var cntr: ControllerProtocol? {
        didSet {
            (cntr as? BaseViewController)?.lifeDelegate = self
        }
    }
    /// a wrapper around navigation controller
    var navigationController: NavigationProtocol? = nil

    /// parent of current coordinator
    var parent: BaseCoordinator? = nil

    init(parent: BaseCoordinator?) {
        self.navigationController = parent?.navigationController
        self.parent = parent
    }

    convenience init() {
        self.init(parent: nil)
    }

    // MARK: - Private

    /// Unique identifier of current coordinator
    private let identifier = UUID()

    /// Dictionary of the child coordinators. Every child coordinator should be added
    /// to that dictionary in order to keep it in memory
    /// Key is an `identifier` of the child coordinator and value is the coordinator itself
    /// Value type is `Any` because Swift doesn't allow to store generic types in the array
    internal var childCoordinators = [UUID: Any]()

    /// Stores coordinator to the `childCoordinators` dictionary
    ///
    /// - Parameter coordinator: Child coordinator to store
    internal func store(coordinator: BaseCoordinator) {
        childCoordinators[coordinator.identifier] = coordinator
    }

    /// Release coordinator from the `childCoordinators` dictionary
    ///
    /// - Parameter coordinator: Coordinator to release
    internal func free(coordinator: BaseCoordinator) {
        childCoordinators[coordinator.identifier] = nil
    }

    // MARK: - Public

    /// 1. attaches given coordinator
    /// 2. If controlled view is set tries to present it
    ///
    /// - Parameter coordinator: Coordinator to start
    func coordinate(to coordinator: BaseCoordinator) {
        attach(coordinator: coordinator)
        if let vc = self.cntr {
            coordinator.presented(on: vc, controller: coordinator.cntr)
        }
    }

    /// 1. Stores coordinator in a dictionary of child coordinators
    /// 2. Calls method `start()` on that coordinator
    ///
    /// - Parameter coordinator: Coordinator to start
    func attach(coordinator: BaseCoordinator) {
        store(coordinator: coordinator)
        coordinator.start()
    }

    /// called if controller should be presented from another vc
    func presented(on vc: ControllerProtocol,
                   controller nvc: ControllerProtocol?) {
        parent?.presented(on: vc, controller: nvc)
    }

    /// Starts job of the coordinator
    ///
    /// - Returns: Result of coordinator job
    func start() {
        fatalError("Start method should be implemented.")
    }

    /// Stops coordinator's children
    func stopChildren() {
        for (_, v) in childCoordinators {
            // emergency situation...
            if let coord = v as? BaseCoordinator {
                coord.stop()
            }
        }
        childCoordinators.removeAll()
    }

    /// Stops current coordinator and its children
    func stop() {
        stopChildren()
    }

    // MARK: - ParentCoordinatorProtocol

    func exitFrom(coordinator: BaseCoordinator, with result: Any?) {
        if let c = childCoordinators[coordinator.identifier] as? BaseCoordinator {
            // it's mine, remove it:
            exit(coordinator: c, with: result)
        } else {
            // not mine, but maybe yours, bro?
            parent?.exitFrom(coordinator: coordinator, with: result)
        }
    }

    func exitFrom(coordinator: CoordinatorProtocol, with result: Any?) {
        if let c = coordinator as? BaseCoordinator {
            exitFrom(coordinator: c, with: result)
        }
    }

    func exit(coordinator: BaseCoordinator, with result: Any?) {
        coordinator.stop()
        free(coordinator: coordinator)
    }

    func broadcast(result: Any?) {
        broadcast(result: result, of: self)
    }

    func broadcast(result: Any?, of coordinator: BaseCoordinator) {
        parent?.broadcast(result: result, of: coordinator)
    }

    // MARK: - BaseDispatcher

    func exitWith(result: Any?) {
        exitFrom(coordinator: self, with: result)
    }
}

extension BaseCoordinator: ViewLifeCycleDelegate {
    public func movedBack() {
        exitWith(result: LifeCycleExit.back)
    }
}
