//
//  CoordinatorProtocol.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol CoordinatorProtocol {
    var cntr: ControllerProtocol? { get }
    func start()
    func stop()
    func presented(on vc: ControllerProtocol, controller: ControllerProtocol?)
}

protocol ParentCoordinatorProtocol: class {
    func exitFrom(coordinator: CoordinatorProtocol, with result: Any?)
}

protocol BaseDispatcherProtocol: class {
    func exitWith(result: Any?)
}

protocol CommonCoordinatorProtocol:
    ParentCoordinatorProtocol,
    BaseDispatcherProtocol,
    AlertProtocol { }
