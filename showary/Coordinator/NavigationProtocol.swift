//
//  NavigationProtocol.swift
//  showary
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol NavigationProtocol {
    func push(_ cntr: ControllerProtocol, animated: Bool)
    func pop(animated: Bool)
}
