//
//  UIViewController+Controller.swift
//  showary
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

extension UIViewController: ControllerProtocol {
    public var vc: UIViewController {
        return self
    }
}
