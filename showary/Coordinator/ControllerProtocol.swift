//
//  ControllerProtocol.swift
//  showary
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ControllerProtocol {
    var vc: UIViewController { get }
}

extension ControllerProtocol {
    var bvc: BaseViewController? {
        return vc as? BaseViewController
    }
}
