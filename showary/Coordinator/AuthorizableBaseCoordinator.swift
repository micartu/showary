//
//  AuthorizableBaseCoordinator.swift
//  showary
//
//  Created by Michael Artuerhof on 28.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import UIKit

class AuthorizableBaseCoordinator: BaseCoordinator {
    private let secret: SecretService

    init(secret: SecretService, parent: BaseCoordinator?) {
        self.secret = secret
        super.init(parent: parent)
    }

    override func broadcast(result: Any?, of coordinator: BaseCoordinator) {
        if let a = result as? Access {
            switch a {
            case .granted:
                if let h = _handler {
                    // remove login screen from stack:
                    navigationController?.pop(animated: true)
                    _handler = nil
                    delay(0.4) {
                        h()
                    }
                }
            }
        } else {
            super.broadcast(result: result, of: coordinator)
        }
    }

    func checkAuthorizationAndExecute(completion: @escaping SimpleHandler) {
        if secret.isAuthCreated {
            completion()
        } else {
            // save handler for using it after authorization will come up
            _handler = completion
            // and then check credentials in login screen:
            let login = LoginCoordinator(parent: self)
            coordinate(to: login)
        }
    }

    private var _handler: SimpleHandler?
}
