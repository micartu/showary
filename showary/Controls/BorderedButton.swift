//
//  BorderedButton.swift
//  showary
//
//  Created by Michael Artuerhof on 13.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

@IBDesignable
class BorderedButton: UIButton {
    @IBInspectable
    var cornerRadius: CGFloat = 5 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.borderWidth = 1
        }
    }

    @IBInspectable
    var foregroundColor: UIColor = .white {
        didSet {
            layer.borderColor = foregroundColor.cgColor
            backgroundColor = foregroundColor
        }
    }
}
