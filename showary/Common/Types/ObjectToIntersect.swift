//
//  ObjectToIntersect.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import ARKit

enum ObjectToIntersect {
    case horizontalPlane
    case verticalPlane

    var resultType: ARHitTestResult.ResultType {
        if self == .horizontalPlane {
            return .estimatedHorizontalPlane
        } else {
            return .estimatedVerticalPlane
        }
    }
}
