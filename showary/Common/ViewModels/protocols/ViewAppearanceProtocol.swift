//
//  ViewAppearanceProtocol.swift
//  showary
//
//  Created by Michael Artuerhof on 06.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol ViewAppearanceProtocol {
    func viewWillAppear()
}
