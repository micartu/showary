//
//  ViewDismissibleProtocol.swift
//  showary
//
//  Created by Michael Artuerhof on 05.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

protocol ViewDismissibleProtocol {
    func dismiss()
}
