//
//  FetchedDataPresenter.swift
//  showary
//
//  Created by Michael Artuerhof on 18/11/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol FetchedDataInteractorInput: class {
    func wrap(entity: Any,
              indexPath: IndexPath,
              with theme: Theme) -> CellAnyModel
}

protocol FetchedDataInteractorOutput: class {
}

class FetchedDataPresenter: CommonViewModel,
    FetchedDataInteractorOutput {

    fileprivate var manager: FetchManagerProtocol

    init(manager: FetchManagerProtocol) {
        self.manager = manager
        self.manager.delegate = self
    }

    // data source
    weak var source: FetchedDataInteractorInput?

    // MARK: FetchResultsInteractorOutput

    // MARK: CommonViewModel

    func getContents() {
        manager.fetchData()
    }

    func refreshContents() {
        // data is already in sync with db, do not worry.
    }

    func selected(indexPath: IndexPath) {
    }

    func model(for indexPath: IndexPath) -> Any {
        guard let source = source else {
            // TODO: add empty model here!
            fatalError("FetchedDataPresenter: source isn't set!")
        }
        let obj = manager.objectAt(indexPath)
        return source.wrap(entity: obj,
                           indexPath: indexPath,
                           with: theme)
    }

    func modelsCount(for section: Int) -> Int {
        return manager.objectsCountFor(section: section)
    }

    func title(for section: Int) -> String {
        return ""
    }

    func sectionsCount() -> Int {
        // multi sections isn't supported yet!
        return 1
    }

    // MARK: members of CommonViewModel protocol
    var theme: Theme! {
        didSet {
            getContents()
        }
    }

    func clean() {}

    var updateItems = Channel<IIndexPathCount?> (value: nil)
    var updateData = Channel<Bool> (value: false)
    var scrollTo = Channel<IMovePath?> (value: nil)
    var removeKeyboard = Channel<Bool> (value: false)
    var busy = Channel<Bool> (value: false)
    var batchUpdate = Channel<Bool?> (value: nil)
    var make = Channel<CellChangeType?> (value: nil)
}

extension FetchedDataPresenter: FetchManagerDelegate {
    func beginBatchUpdate() {
        // would call tableView.beginUpdates() or equivalent
        batchUpdate.set(true)
    }

    func make(change: CellChangeType) {
        switch change {
        case .insert(let index):
            make.set(.insert(index: index))
        case .update(let index):
            make.set(.update(index: index))
        case .move(let from, let to):
            make.set(.move(from: from, to: to))
        case .delete(let index):
            make.set(.delete(index: index))
        }
    }

    func endBatchUpdate() {
        batchUpdate.set(false)
        // switch off batch update
        batchUpdate.set(nil)
    }
}
