//
//  CellAnyModel.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol CellAnyModel: CommonCellModel {
    static var CellAnyType: UIView.Type { get }
    func setupAny(cell: UIView)
}
