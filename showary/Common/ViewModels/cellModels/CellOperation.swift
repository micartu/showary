//
//  CellOperation.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public enum CellOperation {
    case delete
}

public protocol CellOperationProtocol {
    func cellOperation(for indexPath: IndexPath) -> [CellOperation]
    func deleteCell(at indexPath: IndexPath)
}
