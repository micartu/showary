//
//  BigLogoModel.swift
//  showary
//
//  Created by Michael Artuerhof on 11/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol BigLogoCellModel: CommonCell { }

struct BigLogoModel: BigLogoCellModel {
    let theme: Theme
    let id: String
    let image: String
    weak var delegate: BigLogoCellDelegate?
    weak var imgLoader: ImageLoaderService?

	func setup(cell: BigLogoCell) {
        cell.id = id
        let install = { [weak cell] (im: UIImage, refresh: Bool) in
            if !self.isSameCell(cid: cell?.id) {
                return
            }
            if let cell = cell {
                let h = heightFrom(width: cell.imLogo.bounds.width,
                                   proportional: im.size)
                cell.imLogo.image = im
                cell.logoHeightConstraint.constant = h
                if refresh {
                    self.delegate?.update(id: cell.id)
                }
            }
        }
        if image.count > 0 {
            if let av = imgLoader?.loadOffline(image: image) {
                install(av, false)
            } else {
                imgLoader?.load(image: image) { im in
                    install(im, true)
                }
            }
        } else {
            if let av = UIImage(named: "failed") {
                install(av, false)
            }
        }
    }

    func isSameCell(cid: String?) -> Bool {
        if cid == id {
            return true
        }
        return false
    }
}
