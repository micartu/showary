//
//  MultipleSectionPresenter.swift
//  showary
//
//  Created by Michael Artuerhof on 20/08/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol MultipleSectionInteractorInput: class {
    func loadModels(with theme: Theme, section: Int)
}

protocol MultipleSectionInteractorOutput: class {
    func fetched(models: [CellAnyModel], section: Int)
}

open class MultipleSectionPresenter:
    CommonViewModel,
    MultipleSectionInteractorOutput {
    // data source
    weak var source: MultipleSectionInteractorInput?

    init(titles: [String]) {
        self.titles = titles
    }

    // MARK: MultipleSectionInteractorOutput

    func fetched(models: [CellAnyModel], section: Int) {
        self.models[section] = models
        updateData.set(true)
    }

    // MARK: CommonViewModel

    func getContents() {
        for s in titles.indices {
            source?.loadModels(with: theme, section: s)
        }
    }

    func refreshContents() {
        models.removeAll()
        getContents()
    }

    func selected(indexPath: IndexPath) {
    }

    func model(for indexPath: IndexPath) -> Any {
        return (models[indexPath.section])![indexPath.row]
    }

    func modelsCount(for section: Int) -> Int {
        return (models[section])!.count
    }

    func title(for section: Int) -> String {
        return titles[section]
    }

    func sectionsCount() -> Int {
        return models.count
    }

    func clean() {
    }

    // MARK: useful functions
    internal func findModelWith(id: String) -> (CellAnyModel?, IndexPath?) {
        for (k, mm) in models {
            for (i, m) in mm.enumerated() {
                if m.id == id {
                    let ind = IndexPath(row: i, section: k)
                    return (m, ind)
                }
            }
        }
        return (nil, nil)
    }

    // MARK: members of CommonViewModel protocol
    var theme: Theme! {
        didSet {
            getContents()
        }
    }
    var updateItems = Channel<IIndexPathCount?> (value: nil)
    var updateData = Channel<Bool> (value: false)
    var scrollTo = Channel<IMovePath?> (value: nil)
    var removeKeyboard = Channel<Bool> (value: false)
    var busy = Channel<Bool> (value: false)
    var batchUpdate = Channel<Bool?> (value: nil)
    var make = Channel<CellChangeType?> (value: nil)

    // MARK: other members
    internal var titles: [String]
    internal var models = [Int:[CellAnyModel]]()
}
