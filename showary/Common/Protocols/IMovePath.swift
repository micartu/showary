//
//  ChooserProtocols.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

struct IMovePath {
    let row: Int
    let section: Int
    let animated: Bool
}
