//
//  Vectors.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import simd

typealias V3 = SIMD3<Float>
