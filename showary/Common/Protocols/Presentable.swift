//
//  Presentable.swift
//  showary
//
//  Created by Michael Artuerhof on 15.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol Presentable {
    var viewController: UIViewController { get }

    func presentModally(from viewController: UIViewController, completion: (() -> Void)?)
    func showInContainer(_ container: UIView, in viewController: UIViewController)

    func dismissAsNavController()
}

extension Presentable where Self: UIViewController {
    var viewController: UIViewController {
        return self
    }

    func presentModally(from viewController: UIViewController, completion: (() -> Void)?) {
        viewController.present(self, animated: true, completion: completion)
    }

    func showInContainer(_ container: UIView, in viewController: UIViewController) {
        self.view.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(self.view)

        self.view.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        self.view.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        self.view.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
        let bottomConstraint = self.view.bottomAnchor.constraint(equalTo: container.bottomAnchor)
        bottomConstraint.priority = UILayoutPriority(900)
        bottomConstraint.isActive = true

        viewController.addChild(self)
        self.didMove(toParent: viewController)
    }

    func dismissAsNavController() {
        viewController.navigationController?.dismiss(animated: true, completion: nil)
    }
}
