//
//  CommonViewModel.swift
//  CombineExp
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol CommonViewModel {
    var theme: Theme! { get set }
    var updateItems: Channel<IIndexPathCount?> { get }
    var updateData: Channel<Bool> { get }
    var scrollTo: Channel<IMovePath?> { get }
    var busy: Channel<Bool> { get }
    var removeKeyboard: Channel<Bool> { get }
    var batchUpdate: Channel<Bool?> { get }
    var make: Channel<CellChangeType?> { get }

    func getContents()
    func refreshContents()
    func selected(indexPath: IndexPath)
    func model(for indexPath: IndexPath) -> Any
    func modelsCount(for section: Int) -> Int
    func title(for section: Int) -> String
    func sectionsCount() -> Int
    func clean()
}
