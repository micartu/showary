//
//  DictionaryDecoder+fun.swift
//  showary
//
//  Created by Michael Artuerhof on 28.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

func decode<T: Decodable>(_ type: T.Type, from dict: [String: Any]) -> T? {
    let d = DictionaryDecoder()
    return try? d.decode(type, from: dict)
}

func decode<T: Decodable>(_ type: T.Type, from dict: NSDictionary) -> T? {
    if let d = dict as? [String: Any] {
        return decode(type, from: d)
    }
    return nil
}
