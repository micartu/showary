//
//  Channel.swift
//  CombineExp
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

final class Channel<T> {
    private var value: T {
        willSet(v) {
            DispatchQueue.main.async { [weak self] in
                self?.changeClosure?(v)
            }
        }
    }

    typealias NotifierT = ((T) -> Void)?
    private var changeClosure: NotifierT = nil

    init(value: T) {
        self.value = value
    }

    func bind(closure: NotifierT) {
        changeClosure = closure
    }

    func set(_ value: T) {
        self.value = value
    }

    var v: T {
        return value
    }

    func unbind() {
        changeClosure = nil
    }
}
