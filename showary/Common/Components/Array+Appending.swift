//
//  Array+Appending.swift
//  showary
//
//  Copied from: https://github.com/swhitty/DictionaryDecoder
//  Created by Michael Artuerhof on 28.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension Array where Element == CodingKey {

    func appending(key codingKey: CodingKey) -> [CodingKey] {
        var path = self
        path.append(codingKey)
        return path
    }

    func appending(index: Int) -> [CodingKey] {
        var path = self
        path.append(IndexKey(intValue: index))
        return path
    }

    struct IndexKey: CodingKey {
        var intValue: Int? {
            return index
        }

        var stringValue: String {
            return "Index \(index)"
        }

        var index: Int

        init(intValue index: Int) {
            self.index = index
        }

        init?(stringValue: String) {
            return nil
        }
    }
}
