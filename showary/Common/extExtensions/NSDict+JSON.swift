//
//  NSDict+JSON.swift
//  showary
//
//  Created by Michael Artuerhof on 05.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension NSDictionary {
    func toJSONString() -> String? {
        let data = try? JSONSerialization.data(withJSONObject: self,
                                               options: [])
        return String(data: data ?? Data(), encoding: .utf8)
    }
}
