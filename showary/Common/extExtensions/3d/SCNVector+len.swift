//
//  SCNVector+len.swift
//  showary
//
//  Created by Michael Artuerhof on 20.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import SceneKit

extension SCNVector3 {
    func length() -> Float {
        return sqrt((x * x + y * y + z * z) / 3)
    }
}
