//
//  CGPoint+len.swift
//  showary
//
//  Created by Michael Artuerhof on 14.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import SceneKit

extension CGPoint {
    /// Extracts the screen space point from a vector returned by SCNView.projectPoint(_:).
    init(_ vector: SCNVector3) {
        self.init()
        x = CGFloat(vector.x)
        y = CGFloat(vector.y)
    }
    /// Returns the length of a point when considered as a vector. (Used with gesture recognizers.)
    var length: CGFloat {
        return sqrt(x * x + y * y)
    }
}
