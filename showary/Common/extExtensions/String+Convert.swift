//
//  String+Convert.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

extension String {
    var toDouble: Double {
        return Double(self) ?? 0.0
    }
}

extension String {
    var toInt: Int {
        return Int(self) ?? 0
    }
}

extension String {
    var toUInt: UInt {
        return UInt(self) ?? 0
    }
}
