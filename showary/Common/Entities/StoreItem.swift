//
//  StoreItem.swift
//  showary
//
//  Created by Michael Artuerhof on 10.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

struct StoreItem: Codable {
    let id: String
    let name: String
    let img: String
    let descr: String
    let model: String
    let data: String
    let price: Double
    let salePrice: Double
}

extension StoreItem: ModelDescProtocol { }
