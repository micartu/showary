//
//  Model.swift
//  showary
//
//  Created by Michael Artuerhof on 08.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

struct Model {
    let id: String
    let img: String
    let path: String
    let descr: String
}

extension Model: ModelDescProtocol { }
