//
//  User.swift
//  showary
//
//  Created by Michael Artuerhof on 29.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

struct User: Codable {
    let login: String
    let name: String
    var email: String
    var password: String // for auth funcs, should be empty otherwise
}
