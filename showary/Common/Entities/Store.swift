//
//  Store.swift
//  showary
//
//  Created by Michael Artuerhof on 10.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

struct Store: Codable {
    let id: String
    let name: String
    let descr: String
    let logo: String
    let addr: String
    let website: String
}
