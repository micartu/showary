//
//  consts.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

struct Const {
    static let os: String = "ios"

    // error codes
    static let kErrNet = -1
    static let kErrNetAuth = -2
    static let kErrCards = -3

    // intervals
    static let kAnimationTime: TimeInterval = 0.6
    static let kDefaultExpiration: TimeInterval = 60 * 60 * 24 * 7

    static let attached = "_attached_"
    static let root = "_root_"
    static let badge = "_badge_"

    struct notification {
        static let background = "background"
        static let foreground = "foreground"
        static let authErr = "AuthError"
    }

    // error codes
    struct err {
        static let kNet = -100
        static let kNetAuth = -101
        struct fields {
            static let data = "data"
            static let defDescr = "default_description"
            static let kindOfErr = "error"
        }
    }

    struct ar {
        static let alignment = "alignment"
    }

    // texts
    struct text {
        static let kClose = "Close".localized
        static let kOk = "Ok".localized
        static let kCancel = "Cancel".localized
        static let kCancelAction = "CancelAction".localized
        static let kYes = "Yes".localized
        static let kNo = "No".localized
    }
}
