//
//  BigLogoCell.swift
//  showary
//
//  Created by Michael Artuerhof on 11.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import UIKit

protocol BigLogoCellDelegate: class {
    func update(id: String)
}

class BigLogoCell: UITableViewCell {
    @IBOutlet weak var imLogo: UIImageView!
    @IBOutlet weak var logoHeightConstraint: NSLayoutConstraint!
    var id = ""
}
