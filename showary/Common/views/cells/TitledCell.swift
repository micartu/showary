//
//  TitledCell.swift
//  showary
//
//  Created by Michael Artuerhof on 02/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol TitledCellDelegate: class, TouchableCell {
}

final class TitledCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(cellTapped))
        addGestureRecognizer(tap)
    }

    @objc private func cellTapped () {
        delegate?.cellTouched(id: id)
    }

    var id = ""
    weak var delegate: TitledCellDelegate? = nil
}
