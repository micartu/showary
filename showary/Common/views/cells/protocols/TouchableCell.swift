//
//  TouchableCell.swift
//  showary
//
//  Created by Michael Artuerhof on 30.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol TouchableCell {
    func cellTouched(id: String)
}
