//
//  TouchableRoundedBadgedCell.swift
//  showary
//
//  Created by Michael Artuerhof on 30.09.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

final class TouchableRoundedBadgedCell: TouchableAnimatableCell {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var lblBadge: UILabel!
    @IBOutlet weak var imgWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgHeightConstraint: NSLayoutConstraint!
}
