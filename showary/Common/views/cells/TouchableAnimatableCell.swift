//
//  TouchableAnimatableCell.swift
//  showary
//
//  Created by Michael Artuerhof on 22/10/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol TouchableAnimatableCellDelegate: class, TouchableCell {
}

class TouchableAnimatableCell: UICollectionViewCellHitTested {
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(cellTapped))
        addGestureRecognizer(tap)
    }

    @objc internal func cellTapped() {
        let call: (() -> Void) = { [weak self] in
            self?.delegate?.cellTouched(id: self?.id ?? "")
        }
        if let external = touchAction {
            external(self)
            delay(delayedTouch) {
                call()
            }
        } else {
            call()
        }
    }

    var id = ""
    var touchAction: ((TouchableAnimatableCell) -> Void)? = nil
    var delayedTouch: TimeInterval = 0
    weak var delegate: TouchableAnimatableCellDelegate? = nil
}
