//
//  NetFun.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

final class NetFun {
    let netmanager: NetManagerService
    init(netmanager: NetManagerService) {
        self.netmanager = netmanager
    }

    // MARK: - Internal functions

    internal func get(url: String,
                      params: [String: Any]?,
                      success: @escaping ((NSDictionary) -> Void),
                      failure: @escaping ((NSError) -> Void)) {
        put { [weak self] in
            self?.netmanager.getQuery(url: url,
                                      params: params,
                                      success: { (ans: NSDictionary) in
                                        self?.s(ans, success)
                                        self?.executeNext()
            }, failure: { e in
                self?.f(e, failure)
                self?.executeNext()
            })
        }
    }

    internal func get(url: String,
                      success: @escaping (([NSDictionary]) -> Void),
                      failure: @escaping ((NSError) -> Void)) {
        put { [weak self] in
            self?.netmanager.getQuery(url: url,
                                      success: { (ans: [NSDictionary]) in
                                        self?.s(ans, success)
                                        self?.executeNext()
            }, failure: { e in
                self?.f(e, failure)
                self?.executeNext()
            })
        }
    }

    internal func post(url: String, d: Data,
                      success: @escaping ((NSDictionary) -> Void),
                      failure: @escaping ((NSError) -> Void)) {
        put { [weak self] in
            self?.netmanager.postQuery(url: url, data: d,
                                       success: { (ans: NSDictionary) in
                                        self?.s(ans, success)
                                        self?.executeNext()
            }, failure: { e in
                self?.f(e, failure)
                self?.executeNext()
            })
        }
    }

    internal func post(url: String, d: Data,
                       success: @escaping (([NSDictionary]) -> Void),
                       failure: @escaping ((NSError) -> Void)) {
        put { [weak self] in
            self?.netmanager.postQuery(url: url, data: d,
                                       success: { (ans: [NSDictionary]) in
                                        self?.s(ans, success)
                                        self?.executeNext()
            }, failure: { e in
                self?.f(e, failure)
                self?.executeNext()
            })
        }
    }

    // MARK: - Private

    private func put(_ op: @escaping (() -> Void)) {
        lock.lock()
        operations.append(op)
        lock.unlock()
        executeNext()
    }

    private func s<T>(_ ans: T, _ success: ((T) -> Void)?) {
        DispatchQueue.main.async {
            success?(ans)
        }
    }

    private func f(_ err: NSError, _ failure: ((NSError) -> Void)?) {
        DispatchQueue.main.async {
            failure?(err)
        }
    }

    private func executeNext() {
        queue.addOperation { [weak self] in
            let opp: (() -> Void)?
            self?.lock.lock()
            opp = self?.operations.popLast()
            self?.lock.unlock()
            if let op = opp {
                op()
            }
        }
    }

    private lazy var queue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "NetFun Queue"
        queue.qualityOfService = .userInitiated
        return queue
    }()
    private lazy var operations = [() -> Void]()
    internal var user = ""
    private let lock = NSLock()
}
