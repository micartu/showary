//
//  NetManager.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol NetManagerService: NetTransportService, NetAuthService {
    func register(user: User,
                  completion: @escaping ((NSDictionary?, NSError?) -> Void))
}
