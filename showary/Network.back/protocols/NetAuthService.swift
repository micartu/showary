//
//  NetAuthService.swift
//  showary
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol NetAuthService {
    func socialAuth(type: String,
                    token: String,
                    success: ((NSDictionary) -> Void)?,
                    failure: ((NSError) -> Void)?)
    func auth(login: String,
              password: String,
              success: ((NSDictionary) -> Void)?,
              failure: ((NSError) -> Void)?)
    func logout(success: (() -> Void)?,
                failure: ((NSError) -> Void)?)
}
