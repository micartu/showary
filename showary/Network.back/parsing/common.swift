//
//  common.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

func parseArrayOfStrings(_ res: NSDictionary, for key: String) -> [String]? {
    if let tmp = res[key] as? [String] {
        return tmp
    }
    return nil
}

func parseString(_ res: NSDictionary, for key: String) -> String {
    if let tmp = res[key] as? String {
        return tmp
    }
    return ""
}

func parseDouble(_ res: NSDictionary, key: String) -> Double {
    var out: String
    if let tmp = res[key] as? Double {
        return tmp
    }
    if let tmp = res[key] as? String {
        out = tmp
    } else {
        out = "0"
    }
    return out.toDouble
}

func parseInt(_ res: NSDictionary, key: String) -> Int {
    var out: Int
    if let tmp = res[key] as? Int {
        out = tmp
    }
    else if let tmp = res[key] as? String {
        out = tmp.toInt
    }
    else {
        out = 0
    }
    return out
}

func convertDate(df: DateFormatter, date: String) -> NSDate {
    let s: String
    if let range = date.range(of: ".") {
        s = String(date[date.startIndex..<range.lowerBound])
    } else if let range = date.range(of: "+") {
        s = String(date[date.startIndex..<range.lowerBound])
    } else {
        s = date
    }
    if let ddate = df.date(from: s) {
        return ddate as NSDate
    } else {
        return NSDate()
    }
}
