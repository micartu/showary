//
//  NetFun+Auth.swift
//  showary
//
//  Created by Michael Artuerhof on 08.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension NetFun: NetAuthService {
    func auth(login: String, password: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
    }
    
    func logout(success: (() -> Void)?, failure: ((NSError) -> Void)?) {        
    }
}
