//
//  QScannerViewModel.swift
//  showary
//
//  Created by Michael Artuerhof on 23/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation


protocol QScannerDispatcherProtocol: class {
    func showModelWith(id: String)
}

protocol QScannerViewModelProtocol: ViewAppearanceProtocol {
    func handle(code: String)
}

final class QScannerViewModel {
	weak var dispatcher: (AlertProtocol & QScannerDispatcherProtocol)?
	weak var view: QScannerViewProtocol?

    private let net: NetProductsService

    init(net: NetProductsService) {
        self.net = net
    }
}

// MARK: - QScannerViewModelProtocol
extension  QScannerViewModel: QScannerViewModelProtocol {
    func viewWillAppear() {
        dispatcher?.hideBusyIndicator()
        view?.session(resume: true)
    }

    func handle(code: String) {
        view?.session(resume: false)
        let sp = code.split(separator: "|")
        if sp.count > 1 && sp[0] == "_showary_" && sp[1].count > 0 {
            dispatcher?.showModelWith(id: String(sp[1]))
        } else {
            view?.session(resume: true)
        }
    }
}
