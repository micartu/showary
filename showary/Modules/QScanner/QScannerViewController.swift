//
//  QScannerViewController.swift
//  showary
//
//  Created by Michael Artuerhof on 23/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit
import AVFoundation

protocol QScannerViewProtocol: class {
    func session(resume: Bool)
}

final class QScannerViewController: BaseViewController {
    var vm: QScannerViewModelProtocol!

    @IBOutlet weak var borderedView: UIView!
    internal var captureSession = AVCaptureSession()
    internal var photoOutput: AVCaptureOutput!
    internal var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    internal var qrCodeFrameView: UIView?
    private let supportedCodeTypes = [
        AVMetadataObject.ObjectType.qr,
    ]

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            photoOutput = AVCapturePhotoOutput()
        } else {
            let si = AVCaptureStillImageOutput()
            si.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
            photoOutput = si
        }
        setup()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        videoPreviewLayer?.frame = borderedView.layer.bounds
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vm.viewWillAppear()
    }

    // MARK: - Private

    private func setup() {
        let captureDevice: AVCaptureDevice
        if #available(iOS 10.0, *) {
            let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera],
                                                                          mediaType: AVMediaType.video,
                                                                          position: .back)
            guard let cd = deviceDiscoverySession.devices.first else {
                print("Failed to get the camera device")
                return
            }
            captureDevice = cd
        } else {
            guard let cd = AVCaptureDevice.default(for: AVMediaType.video) else {
                print("Failed to get the camera device")
                return
            }
            captureDevice = cd
        }
        do {
            // get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)

            // set the input device on the capture session.
            captureSession.addInput(input)

            // allow capturing metadata
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)

            // also allow taking images for proving if user had cheated or not
            captureSession.addOutput(photoOutput)

            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
        } catch {
            print(error)
            return
        }

        // initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = borderedView.layer.bounds
        borderedView.layer.addSublayer(videoPreviewLayer!)

        // start video capture
        captureSession.startRunning()

        // initialize QR Code Frame to highlight the QR code (or any other supported)
        qrCodeFrameView = UIView()

        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
    }

    fileprivate func continueSession() {
        runOnMainThread { [weak self] in
            self?.qrCodeFrameView?.frame = .zero
            self?.captureSession.startRunning()
        }
    }
}

extension QScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput,
                        didOutput metadataObjects: [AVMetadataObject],
                        from connection: AVCaptureConnection) {
        if let metadataObj = metadataObjects.first as? AVMetadataMachineReadableCodeObject {
            if supportedCodeTypes.contains(metadataObj.type) {
                // if the found metadata is supported then show the green frame around it
                let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
                qrCodeFrameView?.frame = barCodeObject!.bounds
                if let d = metadataObj.stringValue {
                    vm.handle(code: d)
                }
            }
        } else {
            qrCodeFrameView?.frame = .zero
        }
    }
}

// MARK: - QScannerViewProtocol
extension QScannerViewController: QScannerViewProtocol {
    func session(resume: Bool) {
        if resume {
            continueSession()
        } else {
            captureSession.stopRunning()
        }
    }
}

// MARK: - ViewControllerable
extension QScannerViewController: ViewControllerable {
	static var storyboardName: String {
		return "QScanner"
	}
}
