//
//  QScannerCoordinator.swift
//  showary
//
//  Created by Michael Artuerhof on 23/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class QScannerCoordinator: BaseCoordinator {
	override func start() {
		let vc = QScannerViewController.create()
		let viewModel = QScannerViewModel(
            net: ServicesAssembler.inject()
        )
		viewModel.dispatcher = self
		vc.vm = viewModel
        vc.title = "QR-Scanner title".localized
		viewModel.view = vc
		cntr = vc
	}
}

// MARK: - QScannerDispatcherProtocol
extension QScannerCoordinator: QScannerDispatcherProtocol {
    func showModelWith(id: String) {
        let c = ProductInfoCoordinator(parent: self, store: nil, id: id)
        coordinate(to: c)
    }
}
