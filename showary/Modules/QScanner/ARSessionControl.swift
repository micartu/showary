//
//  ARSessionControl.swift
//  showary
//
//  Created by Michael Artuerhof on 23.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

enum ARSessionControl {
    case pause
    case resume
}
