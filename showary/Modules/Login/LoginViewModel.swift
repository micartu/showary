//
//  LoginViewModel.swift
//  showary
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol LoginDispatcherProtocol: class {
    func authorized()
}

protocol LoginViewModelProtocol {
    func loginPressed()
    func togglePassVisibility()
}

protocol LoginViewProtocol: class {
    var login: String { get }
    var password: String { get }

    func set(enabled: Bool)
    func pass(show: Bool)
}

final class LoginViewModel {
    weak var dispatcher: (AlertProtocol & LoginDispatcherProtocol)?
    weak var view: LoginViewProtocol?
    internal let net: NetAuthService
    internal var secret: SecretService
    internal let cache: CacheUserService

    private var passShown = false

    init(net: NetAuthService,
         secret: SecretService,
         cache: CacheUserService) {
        self.net = net
        self.secret = secret
        self.cache = cache
    }
}

extension LoginViewModel: LoginViewModelProtocol {
    func loginPressed() {
        guard let l = view?.login,
            let p = view?.password,
            l.count > 0,
            p.count > 0
            else { return }
        view?.set(enabled: false)
        dispatcher?.showBusyIndicator()
        net.auth(login: l,
                 password: p, success: { [weak self] _ in
                    let u = User(login: l,
                                 name: l,
                                 email: l,
                                 password: p)
                    self?.cache.update(user: u) { // create / update given user in cache
                        self?.view?.set(enabled: true)
                        self?.secret.isAuthCreated = true
                        self?.dispatcher?.hideBusyIndicator()
                        self?.dispatcher?.authorized()
                    }
        }, failure: { [weak self] e in
            self?.view?.set(enabled: true)
            self?.dispatcher?.hideBusyIndicator()
            self?.dispatcher?.show(error: e.localizedDescription)
        })
    }

    func togglePassVisibility() {
        passShown = !passShown
        view?.pass(show: passShown)
    }
}
