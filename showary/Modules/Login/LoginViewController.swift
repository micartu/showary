//
//  LoginViewController.swift
//  showary
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class LoginViewController: BaseViewController {
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!

    private var borderCount = 0

    var vm: LoginViewModelProtocol!

    // MARK: Life cycle

    override func viewDidLoad() {
        let btns = [loginButton, eyeButton]
        for b in btns {
            b?.addTarget(self,
                         action: #selector(buttonPressed(_:)),
                         for: .touchUpInside)
        }
        loginButton.setTitle("Login title".localized, for: .normal)
        lblDescription.text = "Login description".localized
        let fields = [loginField, passwordField]
        let placeholders = ["Login placeholder".localized,
                            "Password placeholder".localized]
        let icons = ["user", "locked"]
        for (i, e) in fields.enumerated() {
            e?.leftView = UIImageView(image: UIImage(named: icons[i]))
            e?.leftViewMode = .always
            let brd = e?.addBorders(edges: [.bottom],
                                    color: UIColor.black,
                                    inset: 0, thickness: 1)
            brd?.forEach {
                $0.tag = (const.border + self.borderCount)
                self.borderCount += 1
            }
            e?.delegate = self
            e?.placeholder = placeholders[i]
        }
        super.viewDidLoad()
        addDismissKeyboard()
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        imgLogo.tintColor = theme.tintColor
        loginButton.setTitleColor(theme.emptyBackColor, for: .normal)
        loginButton.addBorders(width: 1,
                               color: theme.tintColor,
                               cornerRadius: 5)
        for i in 0..<borderCount {
            if let v = view.viewWithTag(const.border + i) {
                v.backgroundColor = theme.prettyColor
            }
        }
        let fields = [loginField, passwordField]
        for e in fields {
            e?.leftView?.tintColor = theme.tintColor
        }
        loginButton.backgroundColor = theme.tintColor
        eyeButton.tintColor = theme.tintColor
    }

    // MARK: Private
    @objc private func buttonPressed(_ sender: UIButton) {
        switch sender {
        case loginButton:
            vm.loginPressed()
        case eyeButton:
            vm.togglePassVisibility()
        default:
            ()
        }
    }

    private struct const {
        static let btn = 313
        static let border = 717
    }
}

// MARK: - UITextFieldDelegate
extension LoginViewController: LoginViewProtocol {
    var login: String {
        return loginField.text ?? ""
    }

    var password: String {
        return passwordField.text ?? ""
    }

    func set(enabled: Bool) {
        loginButton.isEnabled = enabled
    }

    func pass(show: Bool) {
        passwordField.isSecureTextEntry = !show
        let im: String
        if show {
            im = "eye-crossed"
        } else {
            im = "eye"
        }
        eyeButton.setImage(UIImage(named: im), for: .normal)
    }
}

// MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let next: UITextField?
        switch textField {
        case loginField:
            next = passwordField
        case passwordField:
            buttonPressed(loginButton)
            fallthrough
        default:
            next = nil
        }
        next?.becomeFirstResponder()
        return true
    }
}

// MARK: - ViewControllerable
extension LoginViewController: ViewControllerable {
    static var storyboardName: String {
        return "Login"
    }
}
