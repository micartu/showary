//
//  LoginCoordinator.swift
//  showary
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

enum Access {
    case granted
}

final class LoginCoordinator: BaseCoordinator {
    override func start() {
        let vc = LoginViewController.create()
        let viewModel = LoginViewModel(net: ServicesAssembler.inject(),
                                       secret: ServicesAssembler.inject(),
                                       cache: ServicesAssembler.inject())
        viewModel.dispatcher = self
        vc.vm = viewModel
        viewModel.view = vc
        cntr = vc
    }
}

extension LoginCoordinator: LoginDispatcherProtocol {
    func authorized() {
        exitWith(result: nil)
        broadcast(result: Access.granted)
    }
}
