//
//  FirmInfoCoordinator.swift
//  showary
//
//  Created by Michael Artuerhof on 11/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class FirmInfoCoordinator: BaseCoordinator {
    init(parent: BaseCoordinator?, id: String, store: Store? = nil) {
        self.id = id
        self.store = store
        super.init(parent: parent)
    }

	override func start() {
		let vc = FirmInfoViewController.create()
		vc.title = "FirmInfo".localized
        vc.addRefreshControl = false
		vc.initializer = { tv in
            let ids = [
                FirmCaptionCell.self,
                FirmDescrCell.self,
                BigLogoCell.self,
            ]
            for id in ids {
                let identifier = String(describing: id)
                tv.register(UINib(nibName: identifier, bundle: nil),
                            forCellReuseIdentifier: identifier)
            }
            tv.separatorStyle = .none
            //tv.isUserInteractionEnabled = false
		}
        let viewModel = FirmInfoViewModel(id: id,
                                          store: store,
                                          net: ServicesAssembler.inject(),
                                          mdlLoader: ServicesAssembler.inject(),
                                          cache: ServicesAssembler.inject(),
                                          imgLoader: ServicesAssembler.inject())
        viewModel.dispatcher = self
        viewModel.source = viewModel
        vc.viewModel = viewModel
        viewModel.view = vc
        cntr = vc
	}

    fileprivate let id: String
    fileprivate let store: Store?
}

// MARK: - FirmInfoDispatcherProtocol
extension  FirmInfoCoordinator: FirmInfoDispatcherProtocol {
    func show(model: String) {
        broadcast(result: model)
    }
}
