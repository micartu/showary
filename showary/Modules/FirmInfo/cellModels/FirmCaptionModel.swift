//
//  FirmCaptionModel.swift
//  showary
//
//  Created by Michael Artuerhof on 11/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol FirmCaptionCellModel: CommonCell { }

struct FirmCaptionModel: FirmCaptionCellModel {
    let theme: Theme
    let id: String
    let title: String
    let subtitle: String
	weak var delegate: FirmCaptionCellDelegate?
    weak var imgLoader: ImageLoaderService?

	func setup(cell: FirmCaptionCell) {
        cell.id = id
        cell.delegate = delegate

        cell.lblTitle.text = title
        cell.lblTitle.font = theme.boldFont.withSize(theme.sz.middle1)
        cell.lblTitle.textColor = theme.mainTextColor

        cell.lblSubtitle.text = subtitle
        cell.lblSubtitle.font = theme.regularFont.withSize(theme.sz.middle1)
        cell.lblSubtitle.textColor = theme.mainTextColor
    }
}
