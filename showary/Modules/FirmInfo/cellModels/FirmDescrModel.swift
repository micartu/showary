//
//  FirmDescrModel.swift
//  showary
//
//  Created by Michael Artuerhof on 11/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol FirmDescrCellModel: CommonCell { }

struct FirmDescrModel: FirmDescrCellModel {
    let theme: Theme
    let id: String
    let content: String
	weak var delegate: FirmDescrCellDelegate?

	func setup(cell: FirmDescrCell) {
        cell.id = id
        cell.delegate = delegate

        cell.lblContent.text = content
        cell.lblContent.font = theme.regularFont.withSize(theme.sz.middle2)
        cell.lblContent.textColor = theme.mainTextColor
    }
}
