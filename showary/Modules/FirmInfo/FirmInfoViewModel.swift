//
//  FirmInfoViewModel.swift
//  showary
//
//  Created by Michael Artuerhof on 11/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol FirmInfoDispatcherProtocol: class {
    func show(model: String)
}

protocol FirmInfoViewModelProtocol {
}

final class FirmInfoViewModel: SingleSectionPresenter {
	weak var dispatcher: (AlertProtocol & FirmInfoDispatcherProtocol)?
	weak var view: FirmInfoViewProtocol?

	init(id: String,
         store: Store?,
         net: NetFirmService,
         mdlLoader: ModelLoaderService,
         cache: CacheModelService,
         imgLoader: ImageLoaderService) {
        self.id = id
        self.store = store
        self.net = net
        self.mdlLoader = mdlLoader
        self.cache = cache
        self.imgLoader = imgLoader
		super.init()
	}

    fileprivate let id: String
    fileprivate let net: NetFirmService
    fileprivate let mdlLoader: ModelLoaderService
    fileprivate let cache: CacheModelService
    fileprivate let imgLoader: ImageLoaderService
    fileprivate let store: Store?
}

// MARK: - SingleSectionInteractorInput
extension FirmInfoViewModel: SingleSectionInteractorInput {
	func loadModels(with theme: Theme) {
        func install(s: Store) {
            let i = FirmCaptionModel(
                theme: theme,
                id: s.id,
                title: s.name,
                subtitle: s.addr,
                delegate: self,
                imgLoader: self.imgLoader
            )
            let l = BigLogoModel(
                theme: theme,
                id: "",
                image: s.logo,
                delegate: self,
                imgLoader: self.imgLoader
            )
            runOnMainThread {
                self.fetched(models: [i, l])
            }
        }
        if let f = store {
            install(s: f)
        } else { // load from network
            dispatcher?.showBusyIndicator()
            net.getStoreWithID(id, success: { [weak self] s in
                guard let `self` = self else { return }
                self.dispatcher?.hideBusyIndicator()
                install(s: s)
            }, failure: { [weak self] e in
                self?.dispatcher?.hideBusyIndicator()
                self?.dispatcher?.show(error: e.localizedDescription)
            })
        }
	}
}

// MARK: - FirmCaptionCellDelegate
extension FirmInfoViewModel: FirmCaptionCellDelegate {
    func cellTouched(id: String) {
        // TODO: add me
    }
}

// MARK: - FirmDescrCellDelegate
extension FirmInfoViewModel: FirmDescrCellDelegate {
}

// MARK: - FirmInfoViewModelProtocol
extension FirmInfoViewModel: FirmInfoViewModelProtocol {
}

// MARK: - BigLogoCellDelegate
extension FirmInfoViewModel: BigLogoCellDelegate {
    func update(id: String) {
        runOnMainThread { [weak self] in
            if let (_, i) = self?.findModelWith(id: id) {
                if i > -1 {
                    let upd = IIndexPathCount(path: IndexPath(row: i, section: 0),
                                              count: 1,
                                              animated: true)
                    self?.updateItems.set(upd)
                }
            }
        }
    }
}
