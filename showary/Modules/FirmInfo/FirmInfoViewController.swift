//
//  FirmInfoViewController.swift
//  showary
//
//  Created by Michael Artuerhof on 11/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol FirmInfoViewProtocol: class {
}

final class FirmInfoViewController: BaseTableViewController {
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - FirmInfoViewProtocol
extension FirmInfoViewController: FirmInfoViewProtocol {
}

// MARK: - ViewControllerable
extension FirmInfoViewController: ViewControllerable {
	static var storyboardName: String {
		return "FirmInfo"
	}
}
