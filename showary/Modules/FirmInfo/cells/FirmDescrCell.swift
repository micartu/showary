//
//  FirmDescrCell.swift
//  showary
//
//  Created by Michael Artuerhof on 11/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol FirmDescrCellDelegate: class {
}

final class FirmDescrCell: UITableViewCell {
    @IBOutlet weak var lblContent: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    var id = ""
    weak var delegate: FirmDescrCellDelegate? = nil
}
