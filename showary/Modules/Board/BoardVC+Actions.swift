//
//  BoardVC+Actions.swift
//  showary
//
//  Created by Michael Artuerhof on 01.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import UIKit
import SceneKit

extension BoardViewController: UIGestureRecognizerDelegate {
    // MARK: - activity
    @objc func handleBackground() {
        session.pause()
        sessionStopped = true
    }

    @objc func handleForeground() {
        // Prevent the screen from being dimmed to avoid interuppting the AR experience.
        UIApplication.shared.isIdleTimerDisabled = true

        // Start the `ARSession`.
        resetTracking()
        sessionStopped = false
    }

    // MARK: - Interface Actions

    @objc
    internal func buttonAction(_ sender: UIView) {
        switch sender {
        case btnAdd:
            applyTouchAnimation(btnAdd) { [weak self] in
                self?.vm.addObjectAction()
            }
        case btnShowHidePlanes:
            vm.showHidePlanes()
        case btnOK:
            applyTouchAnimation(btnOK) { [weak self] in
                self?.vm.okTouched()
            }
        case btnDelete:
            applyTouchAnimation(imgBtnDelete) { [weak self] in
                self?.vm.deleteTouched()
            }
        case btnStairs:
            applyTouchAnimation(imgBtnStairs) { [weak self] in
                self?.vm.stairsTouched()
            }
        default:
            ()
        }
    }

    // MARK: - Tapping/Rotating actions

    @objc
    internal func didTap(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: sceneView)
        vm.tapped(pt: location)
    }

    @objc
    internal func didPan(_ gesture: ThresholdPanGesture) {
        let location = gesture.location(in: sceneView)
        vm.movedTo(pt: location)
        switch gesture.state {
        case .began:
            vm.panBegan()
        case .changed:
            // Ignore changes to the pan gesture until the threshold for displacment has been exceeded.
            break
        default:
            vm.panEnded()
        }
    }

    @objc
    internal func didRotate(_ gesture: UIRotationGestureRecognizer) {
        guard gesture.state == .changed else { return }
        vm.rotateSelectedObjectWith(angle: gesture.rotation)
        gesture.rotation = 0
    }

    private func applyTouchAnimation(_ sender: UIView,
                                     completion: @escaping (() -> Void)) {
        let anim = CABasicAnimation(keyPath: "transform.scale")
        let kD: Double = 0.25
        anim.fromValue = 1
        anim.toValue = 0.92
        anim.duration = kD
        sender.layer.add(anim, forKey: nil)
        delay(kD) {
            completion()
        }
    }

    /// - Tag: restartExperience
    func restartExperience() {
        guard isRestartAvailable else { return }
        isRestartAvailable = false

        statusViewController.cancelAllScheduledMessages()

        vm.removeAll()
        makeButtons(hidden: true)

        resetTracking()

        // Disable restart for a while in order to give the session time to restart.
        delay(5) { [weak self] in
            self?.isRestartAvailable = true
            self?.makeButtons(hidden: false)
            self?.upperControlsView.isHidden = false
        }
    }
}
