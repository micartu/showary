//
//  BoardViewController.swift
//  showary
//
//  Created by Michael Artuerhof on 01/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import ARKit
import SceneKit
import UIKit

protocol BoardDispatcherProtocol: class {
    var isVirtObjectsSelectionShown: Bool { get }
    func showModels(pointedTo arrowView: UIView,
                    output: AvailModelsOutput?) -> AvailModelsInput
    func dismissPopUps()
    func intoProducts()
}

final class BoardViewController: BaseViewController {
    // MARK: state variables
    weak var dispatcher: (AlertProtocol & BoardDispatcherProtocol)? // TODO: delete me
    var hitTestObject: ObjectToIntersect = .horizontalPlane
    var vm: BoardViewModelProtocol!

    internal var planesHidden = true
    internal var _selector: SCNNode?
    internal var reportScreenCenter = false
    internal var sessionStopped = true
    internal var overlayTimer: Timer?

    // MARK: IBOutlets

    @IBOutlet var sceneView: VirtualObjectARView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var imgBtnDelete: UIImageView!
    @IBOutlet weak var btnStairs: UIButton!
    @IBOutlet weak var imgBtnStairs: UIImageView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var upperControlsView: UIView!
    @IBOutlet weak var btnShowHidePlanes: UIButton!

    // MARK: - UI Elements

    let coachingOverlay = ARCoachingOverlayView()

    /// The view controller that displays the status and "restart experience" UI.
    lazy var statusViewController: StatusViewController = {
        return children.lazy.compactMap({ $0 as? StatusViewController }).first!
    }()

    // MARK: - ARKit Configuration Properties

    /// Marks if the AR experience is available for restart.
    var isRestartAvailable = true

    /// A serial queue used to coordinate adding or removing nodes from the scene.
    let updateQueue = DispatchQueue(label: "com.bearmonti.serialBoardSceneQueue")

    /// Convenience accessor for the session owned by ARSCNView.
    var session: ARSession {
        return sceneView.session
    }

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        #if !targetEnvironment(simulator)
        sceneView.delegate = self
        sceneView.session.delegate = self

        // Set up coaching overlay.
        setupCoachingOverlay()

        // Hook up status view controller callback(s).
        statusViewController.restartExperienceHandler = { [weak self] in
            self?.restartExperience()
        }
        #endif
        [
            btnAdd,
            btnOK,
            btnDelete,
            btnStairs,
            btnShowHidePlanes
        ].forEach({ b in
            b?.addTarget(self,
                         action: #selector(buttonAction(_:)),
                         for: .touchUpInside)
        })
        // gestures
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(didTap(_:)))
        sceneView.addGestureRecognizer(tapGesture)

        let panGesture = ThresholdPanGesture(target: self,
                                             action: #selector(didPan(_:)))
        sceneView.addGestureRecognizer(panGesture)

        let rotationGesture = UIRotationGestureRecognizer(target: self,
                                                          action: #selector(didRotate(_:)))
        sceneView.addGestureRecognizer(rotationGesture)
        // background/foreground events
        NotificationCenter.default.addObserver(self, selector: #selector(handleBackground),
                                               name: NSNotification.Name(rawValue: Const.notification.background),
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleForeground),
                                               name: NSNotification.Name(rawValue: Const.notification.foreground),
                                               object: nil)
        handleForeground()
        vm.viewIsReady()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController?.delegate = self
        if sessionStopped {
            handleForeground()
        }
        hideBusyIndicator()
    }

    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }

    // MARK: - Theming

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        btnAdd.tintColor = theme.mainBackColor
        btnOK.tintColor = theme.mainBackColor
        imgBtnDelete.tintColor = theme.emptyBackColor
        imgBtnStairs.tintColor = theme.emptyBackColor
    }

    // MARK: - Session management

    /// Creates a new AR configuration to run on the `session`.
    func resetTracking() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal, .vertical]
        if #available(iOS 12.0, *) {
            configuration.environmentTexturing = .automatic
        }
        session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }

    // MARK: - Error handling
    func displayErrorMessage(title: String, message: String) {
        // Blur the background.
        blurView.isHidden = false
        show(title: title, message: message)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - UINavigationControllerDelegate
extension BoardViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              willShow viewController: UIViewController,
                              animated: Bool) {
        if viewController == self {
            navigationController
                .setNavigationBarHidden(true, animated: animated)
        }
    }
}

// MARK: - ViewControllerable
extension BoardViewController: ViewControllerable {
	static var storyboardName: String {
		return "Board"
	}
}
