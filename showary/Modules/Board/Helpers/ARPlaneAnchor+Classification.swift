//
//  ARPlaneAnchor+Classification.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import ARKit

@available(iOS 12.0, *)
extension ARPlaneAnchor.Classification {
    var description: String {
        switch self {
        case .wall:
            return "Wall".localized
        case .floor:
            return "Floor".localized
        case .ceiling:
            return "Ceiling".localized
        case .table:
            return "Table".localized
        case .seat:
            return "Seat".localized
        case .none(.unknown):
            return "Unknown".localized
        default:
            return ""
        }
    }
}
