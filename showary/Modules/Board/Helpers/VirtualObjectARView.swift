//
//  VirtualObjectARView.swift
//  showary
//
//  Created by Michael Artuerhof on 01.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation
import ARKit

class VirtualObjectARView: ARSCNView {
    func existingObjectContainingNode(_ node: SCNNode) -> VirtualObject? {
        if let virtualObjectRoot = node as? VirtualObject {
            return virtualObjectRoot
        }

        guard let parent = node.parent else { return nil }

        // Recurse up to check if the parent is a `VirtualObject`.
        return existingObjectContainingNode(parent)
    }

    // - MARK: Object anchors
    /// - Tag: AddOrUpdateAnchor
    func addOrUpdateAnchor(for object: VirtualObject) {
        // If the anchor is not nil, remove it from the session.
        if let anchor = object.anchor {
            session.remove(anchor: anchor)
        }

        // Create a new anchor with the object's current transform and add it to the session
        let newAnchor = ARAnchor(transform: object.simdWorldTransform)
        object.anchor = newAnchor
        session.add(anchor: newAnchor)
    }
}

extension ARSCNView {
    var screenCenter: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
}
