//
//  BoardVC+World.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

extension BoardViewController: WorldProtocol {
    func planes(at point: CGPoint) -> [IntersectionProtocol] {
        return sceneView.hitTest(
            point,
            options: [
                .categoryBitMask: VirtualObjectType.planes,
            ]
        )
    }

    func objects(at point: CGPoint) -> [IntersectionProtocol] {
        return sceneView.hitTest(
            point,
            options: [
                .categoryBitMask: VirtualObjectType.visibleMask,
                .boundingBoxOnly: true,
            ]
        )
    }

    fileprivate func findIntersection(_ pt: CGPoint, type: ARHitTestResult.ResultType) -> V3? {
        let hitTest = sceneView.hitTest(pt, types: type)
        if let r = hitTest.first {
            return r.worldTransform.translation
        }
        return nil
    }

    func findHorizontalPlaneFor(_ pt: CGPoint) -> V3? {
        return findIntersection(pt, type: .estimatedHorizontalPlane)
    }

    func findVerticalPlaneFor(_ pt: CGPoint) -> V3? {
        return findIntersection(pt, type: .estimatedVerticalPlane)
    }

    func findFeaturePointFor(_ pt: CGPoint) -> V3? {
        return findIntersection(pt, type: .featurePoint)
    }

    func rootObject(obj: SObjectProtocol) -> SObjectProtocol {
        if let node = obj as? SCNNode,
            let v = sceneView.existingObjectContainingNode(node) {
            return v
        }
        return obj
    }

    func attach(_ obj: SObjectProtocol) {
        if let n = obj as? SCNNode {
            if let v = sceneView.existingObjectContainingNode(n) {
                sceneView.addOrUpdateAnchor(for: v)
            }
            sceneView.scene.rootNode.addChildNode(n)
        }
    }

    func findRefenceFor(obj: SObjectProtocol) -> URL? {
        if let n = obj as? SCNNode {
            return sceneView.existingObjectContainingNode(n)?.referenceURL
        }
        return nil
    }

    func setAdditionalInfoFor(obj: SObjectProtocol, from dict: [String:String]?) {
        if let d = dict,
            let n = obj as? SCNNode,
            let v = sceneView.existingObjectContainingNode(n) {
            v.caption = d["title"]
        }
    }

    func showAdditionalInfoFor(obj: SObjectProtocol) {
        if let n = obj as? SCNNode,
            let v = sceneView.existingObjectContainingNode(n) {
            if let caption = v.caption {
                statusViewController.showDescription(caption)
            }
        }
    }

    func hideAddionalInfo() {
        statusViewController.showDescription("")
    }

    func attachOrChangeSelector(pos: V3) {
        if _selector == nil {
            let cx: CGFloat = 0.1
            let shape = SCNTube(innerRadius: cx,
                                outerRadius: cx * 1.1,
                                height: 0.001)
            shape.firstMaterial?.diffuse.contents = theme.allowedColor
            let node = SCNNode(geometry: shape)
            node.categoryBitMask = VirtualObjectType.sel.rawValue
            node.position = SCNVector3(pos)
            sceneView.scene.rootNode.addChildNode(node)
            _selector = node
        }
        _selector?.position = SCNVector3(pos)
    }

    func detachSelector() {
        _selector?.removeFromParentNode()
        _selector = nil
    }

    func addSelectionBadgeAt(_ pos: V3, for obj: SObjectProtocol) {
        if let n = obj as? SCNNode {
            let b = n.boundingBox
            let s = n.scale.length()
            let cx = CGFloat(max(b.max.x - b.min.x, b.max.z - b.min.z) * s) * 0.6
            let shape = SCNTube(innerRadius: cx,
                                outerRadius: cx * 1.1,
                                height: 0.001)
            shape.firstMaterial?.diffuse.contents = UIColor.red
            let node = SCNNode(geometry: shape)
            node.name = Const.badge
            node.categoryBitMask = VirtualObjectType.badge.rawValue
            node.position = SCNVector3(pos)
            attachHeartAnimationTo(shape, node: node)
            sceneView.scene.rootNode.addChildNode(node)
        }
    }

    fileprivate func attachHeartAnimationTo(_ s: SCNTube, node: SCNNode) {
        let keys = ["innerRadius", "outerRadius"]
        let start: [CGFloat] = [s.innerRadius, s.outerRadius]
        for (i, k) in keys.enumerated() {
            let key = "geometry." + k
            let heartbeat = CABasicAnimation(keyPath: key)
            heartbeat.duration = 1.5
            heartbeat.fromValue = start[i]
            heartbeat.toValue = start[i] * 1.1
            heartbeat.autoreverses = true
            heartbeat.repeatCount = .infinity
            node.addAnimation(heartbeat, forKey: key)
        }
    }

    fileprivate var badges: [SCNNode] {
        return sceneView.scene.rootNode
            .childNodes(passingTest: { n, _ in n.name == Const.badge })
    }

    func updateBadgesPos(_ pos: V3) {
        badges.forEach { $0.position = SCNVector3(pos) }
    }

    func removeSelectionBadge() {
        badges.forEach { $0.removeFromParentNode() }
    }
}
