//
//  BoardVC+VM.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import UIKit
import SceneKit

extension BoardViewController: BoardViewProtocol {
    func set(planesHidden: Bool) {
        btnShowHidePlanes.tintColor = planesHidden ?
            theme.inactiveColor : theme.tintColor
        self.planesHidden = planesHidden
        sceneView.scene.rootNode
            .childNodes(passingTest: { node, _ in node is Plane })
            .forEach({ ($0 as? Plane)?.set(hidden: planesHidden) })
    }

    func show(hint: String) {
        statusViewController.cancelAllScheduledMessages()
        statusViewController.showMessage(hint)
    }

    func makePanel(hidden: Bool,
                   animated: Bool,
                   types: [manipulationPanelType],
                   delayed: TimeInterval,
                   completion: @escaping (() -> Void)) {
        func makeIT() {
            func check(type: manipulationPanelType,
                       button: UIView,
                       transform tr: CGAffineTransform = .identity) {
                if types.contains(type) {
                    button.transform = tr
                }
            }
            if hidden {
                let sz = UIScreen.main.bounds.size
                let h = sz.height * 1.5
                let tr = CGAffineTransform(translationX: 0, y: h)
                check(type: .deletion, button: btnDelete, transform: tr)
                check(type: .deletion, button: imgBtnDelete, transform: tr)
                check(type: .stairs, button: btnStairs, transform: tr)
                check(type: .stairs, button: imgBtnStairs, transform: tr)
                check(type: .approval, button: btnOK, transform: tr)
                check(type: .addition, button: btnAdd, transform: tr)
            } else {
                check(type: .deletion, button: btnDelete)
                check(type: .deletion, button: imgBtnDelete)
                check(type: .stairs, button: btnStairs)
                check(type: .stairs, button: imgBtnStairs)
                check(type: .approval, button: btnOK)
                check(type: .addition, button: btnAdd)
            }
        }
        if animated {
            UIView.animate(withDuration: 0.3, delay: delayed, animations: {
                makeIT()
            }, completion: { _ in
                completion()
            })
        } else {
            makeIT()
            completion()
        }
    }

    func screenCenter(report: Bool) {
        reportScreenCenter = report
        if !report {
            detachSelector()
        }
    }

    func make(marked: Bool,
              panel: manipulationPanelType) {
        let color = marked ? theme.tintColor : theme.emptyBackColor
        switch panel {
        case .deletion:
            imgBtnDelete.tintColor = color
        case .stairs:
            imgBtnStairs.tintColor = color
        default:
            () // we cannot change state of other buttons
        }
    }
}
