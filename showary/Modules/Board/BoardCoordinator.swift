//
//  BoardCoordinator.swift
//  showary
//
//  Created by Michael Artuerhof on 01/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class BoardCoordinator: BaseCoordinator {
	override func start() {
		let vc = BoardViewController.create()
        let vm = BoardViewModel(
            world: vc,
            view: vc,
            effector: ServicesAssembler.inject(),
            cache: ServicesAssembler.inject(),
            mdlLoader: ServicesAssembler.inject()
        )
        vm.dispatcher = self
        vc.vm = vm
		cntr = vc
	}

    override func broadcast(result: Any?, of coordinator: BaseCoordinator) {
        if let id = result as? String { // model_id
            for (_, c) in childCoordinators {
                if let child = c as? BaseCoordinator {
                    child.stop()
                }
            }
            if let nav = navigationController as? UINavigationController,
                let vc = cntr?.vc {
                nav.setViewControllers([vc], animated: true)
            }
            (cntr?.vc as? BoardViewController)?.vm.load(modelId: id)
        } else if let session = result as? ARSessionControl {
            guard let vc = cntr?.vc as? BoardViewController else {
                return
            }
            switch session {
            case .pause:
                vc.handleBackground()
            case .resume:
                vc.handleForeground()
            }
        } else {
            super.broadcast(result: result, of: coordinator)
        }
    }

    var isVirtObjectsSelectionShown = false
}

// MARK: - BoardDispatcherProtocol
extension BoardCoordinator: BoardDispatcherProtocol {
    func showModels(pointedTo arrowView: UIView,
                    output: AvailModelsOutput?) -> AvailModelsInput {
        let mc = AvailModelsCoordinator(parent: self)
        attach(coordinator: mc)
        if let pv = mc.cntr?.vc {
            pv.modalPresentationStyle = .popover
            if let pop = pv.popoverPresentationController {
                pop.sourceView = arrowView
                pop.sourceRect = arrowView.bounds
                pop.delegate = cntr?.vc as? UIPopoverPresentationControllerDelegate
                #if false // maybe you'd want change the default sizes of the control
                pv.preferredContentSize = CGSize(width: 400,
                                                 height: 300)
                #endif
            }
            isVirtObjectsSelectionShown = true
            cntr?.vc.present(pv, animated: true)
        }
        return mc
    }

    func dismissPopUps() {
        isVirtObjectsSelectionShown = false
        cntr?.vc
            .presentedViewController?
            .dismiss(animated: true)
        stopChildren()
    }

    func intoProducts() {
        dismissPopUps()
        let c = ProductsCoordinator(parent: self)
        coordinate(to: c)
    }
}
