//
//  SCNNode+SObj.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import SceneKit

extension SCNNode: SObjectProtocol {
    var objectName: String {
        get {
            return name ?? ""
        }
        set {
            name = newValue
        }
    }

    var pos: V3 {
        get {
            return SIMD3(position.x, position.y, position.z)
        }
        set {
            position = SCNVector3(newValue)
        }
    }

    var objects: [SObjectProtocol] {
        return childNodes
    }

    func rotateAroundY(_ angle: Float) {
        eulerAngles.y -= angle
    }

    func animateAroundY() {
        let cy: CGFloat = 0.01
        let duration: TimeInterval = 0.3
        let moveUP = SCNAction.moveBy(x: 0, y: cy, z: 0, duration: duration)
        moveUP.timingMode = .easeInEaseOut
        let moveDown = SCNAction.moveBy(x: 0, y: -cy, z: 0, duration: duration)
        moveDown.timingMode = .easeInEaseOut
        let seq = SCNAction.sequence([moveUP, moveDown])
        let loop = SCNAction.repeatForever(seq)
        runAction(loop, forKey: Const.root)
    }

    func stopRunningAnimations() {
        removeAction(forKey: Const.root)
        removeAllAnimations()
    }

    func attach(_ obj: SObjectProtocol) {
        if let o = obj as? SCNNode {
            if let n = name {
                if !n.contains(Const.attached) {
                    name = Const.attached + ":" + n
                }
            } else {
                name = Const.attached
            }
            addChildNode(o)
        }
    }

    func detach() {
        removeFromParentNode()
    }
}
