//
//  ARHitTestResult+Intersection.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import ARKit

extension SCNHitTestResult: IntersectionProtocol {
    var local: V3 {
        return V3(localCoordinates)
    }

    var global: V3 {
        return V3(worldCoordinates)
    }

    var obj: SObjectProtocol {
        return node
    }
}
