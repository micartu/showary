//
//  manipulationPanelType.swift
//  showary
//
//  Created by Michael Artuerhof on 21.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

enum manipulationPanelType {
    case deletion
    case stairs
    case approval
    case addition
}
