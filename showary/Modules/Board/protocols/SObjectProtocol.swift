//
//  SObjectProtocol.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import simd

/// describes a surface object
protocol SObjectProtocol {
    /// name of object
    var objectName: String { get set }

    /// object's position in world
    var pos: V3 { get set }

    /// returns children of objects attached to the current one
    var objects: [SObjectProtocol] { get }

    func rotateAroundY(_ angle: Float)

    func animateAroundY()

    func stopRunningAnimations()

    /// attaches given object to the current one
    func attach(_ obj: SObjectProtocol)

    /// detaches object from its parent
    func detach()
}

func != (lhs: SObjectProtocol, rhs: SObjectProtocol) -> Bool {
    return (lhs.objectName != rhs.objectName &&
        lhs.pos != rhs.pos)
}
