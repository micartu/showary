//
//  IntersectionProtocol.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import simd

protocol IntersectionProtocol {
    var local: V3 { get }
    var global: V3 { get }
    var obj: SObjectProtocol { get }
}
