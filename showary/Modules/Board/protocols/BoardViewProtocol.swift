//
//  BoardViewProtocol.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

protocol BoardViewProtocol: class {
    func set(planesHidden: Bool)
    func show(hint: String)
    func makeManipulationPanel(hidden: Bool,
                               animated: Bool,
                               completion: @escaping (() -> Void))
    func makePanel(hidden: Bool,
                   animated: Bool,
                   types: [manipulationPanelType],
                   delayed: TimeInterval,
                   completion: @escaping (() -> Void))
    func make(marked: Bool,
              panel: manipulationPanelType)
    func screenCenter(report: Bool)
}

extension BoardViewProtocol {
    func makeManipulationPanel(hidden: Bool,
                               animated: Bool,
                               completion: @escaping (() -> Void)) {
        let g = DispatchGroup()
        g.enter()
        makePanel(hidden: hidden, animated: animated, types: [.approval], delayed: 0, completion: { g.leave() })
        g.enter()
        makePanel(hidden: hidden, animated: animated, types: [.deletion, .stairs], delayed: 0.1, completion: { g.leave() })
        g.enter()
        makePanel(hidden: !hidden, animated: animated, types: [.addition], delayed: 0, completion: { g.leave() })
        g.notify(queue: .main) {
            completion()
        }
    }

    func makeManipulationPanel(hidden: Bool,
                               animated: Bool) {
        makeManipulationPanel(hidden: hidden, animated: animated, completion: {})
    }

    func makePanel(hidden: Bool,
                   animated: Bool,
                   types: [manipulationPanelType]) {
        makePanel(hidden: hidden, animated: animated, types: types, delayed: 0, completion: {})
    }

    func makePanel(hidden: Bool,
                   animated: Bool,
                   types: [manipulationPanelType],
                   delayed: TimeInterval) {
        makePanel(hidden: hidden, animated: animated, types: types, delayed: delayed, completion: {})
    }
}
