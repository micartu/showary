//
//  BoardViewModelProtocol.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import UIKit

protocol BoardViewModelProtocol: class {
    func viewIsReady()
    /// add new object action
    func addObjectAction()
    /// show/hide manipulation panels
    func showHidePlanes()
    /// used tapped ok button in oder to fix the position of object and deselect it
    func okTouched()
    /// user tapped delete button in order to delete object
    func deleteTouched()
    /// user wants to move object up/down
    func stairsTouched()
    /// pan events
    func panBegan()
    func panEnded()
    /// center of screen coordinates
    func centered(pt: CGPoint)

    /// user wants to load model with given id
    func load(modelId: String)

    /// user tapped on the screen
    func tapped(pt: CGPoint)
    /// user moved touch on the screen
    func movedTo(pt: CGPoint)
    /// rotate an active object on given amount
    func rotateSelectedObjectWith(angle: CGFloat)
    /// select object in any of given coordinates
    func selectObjectIn(coordinates: [CGPoint])
    /// hide/show all nodes added
    func makeAll(hidden: Bool)
    /// removes all loaded nodes
    func removeAll()
    /// which object are in the scene
    func loadedVirtualObjects() -> [VirtualObject]
}
