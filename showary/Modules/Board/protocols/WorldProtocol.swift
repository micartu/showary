//
//  WorldProtocol.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import UIKit

protocol WorldProtocol: class {
    var hitTestObject: ObjectToIntersect { get set }
    func objects(at point: CGPoint) -> [IntersectionProtocol]
    func planes(at point: CGPoint) -> [IntersectionProtocol]
    func rootObject(obj: SObjectProtocol) -> SObjectProtocol
    func findHorizontalPlaneFor(_ pt: CGPoint) -> V3?
    func findVerticalPlaneFor(_ pt: CGPoint) -> V3?
    func findFeaturePointFor(_ pt: CGPoint) -> V3?
    func attach(_ obj: SObjectProtocol)
    func findRefenceFor(obj: SObjectProtocol) -> URL?
    func setAdditionalInfoFor(obj: SObjectProtocol, from dict: [String:String]?)
    func showAdditionalInfoFor(obj: SObjectProtocol)
    func hideAddionalInfo()
    func attachOrChangeSelector(pos: V3)
    func addSelectionBadgeAt(_ pos: V3, for obj: SObjectProtocol)
    func updateBadgesPos(_ pos: V3)
    func removeSelectionBadge()
}
