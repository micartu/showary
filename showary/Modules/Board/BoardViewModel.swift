//
//  BoardViewModel.swift
//  showary
//
//  Created by Michael Artuerhof on 19.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import UIKit

/// used to control position and rotation of objects within the user's space
final class BoardViewModel {
    weak var dispatcher: (AlertProtocol & BoardDispatcherProtocol)?
    internal weak var world: WorldProtocol?
    internal weak var view: BoardViewProtocol?
    internal var addingMode = false
    internal var showPlanes = false
    internal var stairsMode = false
    internal var lastY: CGFloat = 0
    internal var selectedObj: SObjectProtocol?
    internal let mdlLoader: ModelLoaderService
    internal let cache: CacheModelService
    internal let effector: SoundBoxService
    internal var modelId = ""
    internal var sndToPlay = ""
    internal var loaded = [VirtualObject]()
    internal var coordinates: V3?

    init(world: WorldProtocol?,
         view: BoardViewProtocol?,
         effector: SoundBoxService,
         cache: CacheModelService,
         mdlLoader: ModelLoaderService) {
        self.world = world
        self.view = view
        self.effector = effector
        self.cache = cache
        self.mdlLoader = mdlLoader
    }

    private struct const {
        static let offset: Float = 0.05
    }
}

// MARK: - BoardViewModelProtocol
extension BoardViewModel: BoardViewModelProtocol {
    // MARK interface methods
    func viewIsReady() {
        view?.set(planesHidden: !showPlanes)
        view?.makeManipulationPanel(hidden: true, animated: false)
    }

    func addObjectAction() {
        dispatcher?.intoProducts()
    }

    func showHidePlanes() {
        showPlanes = !showPlanes
        view?.set(planesHidden: !showPlanes)
    }

    func okTouched() {
        if addingMode {
            view?.screenCenter(report: false)
            addingMode = false
            view?.makeManipulationPanel(hidden: true, animated: true)
            if let x = coordinates {
                attachObjTo(x)
            } else {
                view?.show(
                    hint: "The plane where to place the object wasn't found".localized
                )
            }
        } else if selectedObj != nil {
            view?.makeManipulationPanel(hidden: true, animated: true) { [weak self] in
                self?.deselect()
            }
        }
    }

    func deleteTouched() {
        if let s = selectedObj {
            deselect(sound: "deleted")
            if let ref = world?.findRefenceFor(obj: s) {
                loaded.removeAll(where: {
                    $0.referenceURL == ref
                })
            }
            s.detach()
        }
    }

    func stairsTouched() {
        stairsMode = !stairsMode
        view?.make(marked: stairsMode, panel: .stairs)
    }

    // MARK world manipulation methods

    fileprivate func attachObjTo(_ pt: SIMD3<Float>) {
        if modelId.count > 0 {
            if let m = cache.modelWith(id: modelId) {
                let opts = [String:String](fromJSONString: m.descr)
                mdlLoader.loadModel(
                    named: m.path,
                    options: opts) { [weak self] (obj, err) in
                        self?.addingMode = false
                        if let o = obj {
                            o.pos = pt
                            self?.loaded.append(o)
                            self?.world?.setAdditionalInfoFor(obj: o, from: opts)
                            self?.effector.play(sound: "inserted")
                            self?.world?.attach(o)
                        } else if let e = err {
                            self?.dispatcher?.show(error: e.localizedDescription)
                        }
                }
            }
        }
    }

    func load(modelId: String) {
        self.modelId = modelId
        addingMode = true
        view?.show(
            hint: "Find a plane where you want to place the object".localized
        )
        view?.screenCenter(report: true)
        view?.makePanel(hidden: true, animated: true, types: [.addition, .deletion])
        view?.makePanel(hidden: false, animated: true, types: [.approval])
    }

    func tapped(pt: CGPoint) {
        if !addingMode {
            if selectedObj == nil {
                selectObjectIn(coordinates: [pt])
            } else {
                deselect()
                tapped(pt: pt)
            }
        }
    }

    func panBegan() {
        panEnded()
    }

    func panEnded() {
        if stairsMode {
            lastY = 0
        }
    }

    func movedTo(pt: CGPoint) {
        if var s = selectedObj {
            if stairsMode {
                let dY: Float
                if abs(lastY) > 0.01 {
                    dY = Float(lastY - pt.y) * 0.001
                } else {
                    dY = 0
                }
                s.pos.y += dY
                lastY = pt.y
                return
            }
            let px: V3?
            if let x = world?.planes(at: pt).first {
                px = x.global
            } else if let x = world?.findHorizontalPlaneFor(pt) {
                px = x
            } else if let x = world?.findVerticalPlaneFor(pt) {
                px = x
            } else {
                px = nil
            }
            if var x = px {
                world?.updateBadgesPos(x)
                x.y += const.offset
                s.pos = x
            }
        } else {
            selectObjectIn(coordinates: [pt])
        }
    }

    func centered(pt: CGPoint) {
        if addingMode {
            // calculate position to plane and change selector's position:
            let coor: V3?
            if let x = world?.planes(at: pt).first {
                coor = x.global
            } else if let x = world?.findHorizontalPlaneFor(pt) {
                coor = x
            } else if let x = world?.findVerticalPlaneFor(pt) {
                coor = x
            } else {
                coor = nil
            }
            if let x = coor {
                coordinates = x
                world?.attachOrChangeSelector(pos: x)
            }
        }
    }

    func selectObjectIn(coordinates: [CGPoint]) {
        for pt in coordinates {
            if let xs = world?.objects(at: pt) {
                for x in xs {
                    if x.obj.objectName == Const.badge { continue }
                    if let root = world?.rootObject(obj: x.obj),
                        !(root is Plane) {
                        selectedObj = root
                        selectedObj?.animateAroundY()
                        var pos = root.pos
                        world?.addSelectionBadgeAt(pos, for: root)
                        // lift object a little bit to give him some space:
                        pos.y += const.offset
                        selectedObj?.pos = pos
                        world?.showAdditionalInfoFor(obj: root)
                        view?.makeManipulationPanel(hidden: false, animated: true)
                        return
                    }
                }
            }
        }
    }

    func deselect(sound: String = "put-down") {
        selectedObj?.stopRunningAnimations()
        world?.removeSelectionBadge()
        world?.hideAddionalInfo()
        if sound.count > 0 {
            effector.play(sound: sound)
        }
        // place the object back to ground...
        selectedObj?.pos.y -= const.offset
        selectedObj = nil
        view?.makeManipulationPanel(hidden: true, animated: true)
    }

    func rotateSelectedObjectWith(angle: CGFloat) {
        if let obj = selectedObj {
            obj.rotateAroundY(Float(angle))
        }
    }

    func makeAll(hidden: Bool) {
        loaded.forEach {
            $0.opacity = hidden ? 0 : 1
        }
    }

    func removeAll() {
        loaded.forEach {
            $0.removeFromParentNode()
        }
        loaded.removeAll()
    }

    func loadedVirtualObjects() -> [VirtualObject] {
        return loaded
    }
}
