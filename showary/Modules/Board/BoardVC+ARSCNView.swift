//
//  BoardVC+ARSCNView.swift
//  showary
//
//  Created by Michael Artuerhof on 01.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation
import ARKit

extension BoardViewController: ARSCNViewDelegate, ARSessionDelegate {

    // MARK: - ARSCNViewDelegate

    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        if reportScreenCenter {
            DispatchQueue.main.async { [weak self] in
                if let pt = self?.sceneView.screenCenter {
                    self?.vm.centered(pt: pt)
                }
            }
        }
        #if false
        let isAnyObjectInView = virtualObjectLoader.loadedObjects.contains { object in
            return sceneView.isNode(object, insideFrustumOf: sceneView.pointOfView!)
        }

        DispatchQueue.main.async { [weak self] in
            self?.updateFocusSquare(isObjectVisible: isAnyObjectInView)
        }
        #endif
    }

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        // Place content only for anchors found by plane detection.
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }

        // Create a custom object to visualize the plane geometry and extent.
        let plane = Plane(anchor: planeAnchor, in: sceneView, hidden: planesHidden)

        // Add the visualization to the ARKit-managed node so that it tracks
        // changes in the plane anchor as plane estimation continues.
        DispatchQueue.main.async {
            node.addChildNode(plane)
            #if false
            self?.statusViewController.cancelScheduledMessage(for: .planeEstimation)
            self?.statusViewController.showMessage("SURFACE DETECTED".localized)
            if let isEmpty = self?.virtualObjectLoader.loadedObjects.isEmpty, isEmpty {
                self?.statusViewController.scheduleMessage(
                    "TAP + TO PLACE AN OBJECT".localized,
                    inSeconds: 7.5,
                    messageType: .contentPlacement
                )
            }
            #endif
        }
    }

    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        updateQueue.async { [weak self] in
            // Update only anchors and nodes set up by `renderer(_:didAdd:for:)`.
            guard let planeAnchor = anchor as? ARPlaneAnchor,
                let plane = node.childNodes.first as? Plane
                else { return }

            // Update ARSCNPlaneGeometry to the anchor's new estimated shape.
            if let planeGeometry = plane.meshNode.geometry as? ARSCNPlaneGeometry {
                planeGeometry.update(from: planeAnchor.geometry)
            }

            // Update extent visualization to the anchor's new bounding rectangle.
            if let extentGeometry = plane.extentNode.geometry as? SCNPlane {
                extentGeometry.width = CGFloat(planeAnchor.extent.x)
                extentGeometry.height = CGFloat(planeAnchor.extent.z)
                plane.extentNode.simdPosition = planeAnchor.center
            }

            // Update the plane's classification and the text position
            if #available(iOS 12.0, *),
                let classificationNode = plane.classificationNode,
                let classificationGeometry = classificationNode.geometry as? SCNText {
                let currentClassification = planeAnchor.classification.description
                if let oldClassification = classificationGeometry.string as? String,
                    oldClassification != currentClassification {
                    classificationGeometry.string = currentClassification
                    classificationNode.centerAlign()
                }
            }
            if let objectAtAnchor = self?.vm.loadedVirtualObjects().first(where: { $0.anchor == anchor }) {
                objectAtAnchor.simdPosition = anchor.transform.translation
                objectAtAnchor.anchor = anchor
            }
        }
    }

    /// - Tag: ShowVirtualContent
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        statusViewController.showTrackingQualityInfo(for: camera.trackingState, autoHide: true)
        switch camera.trackingState {
        case .notAvailable, .limited:
            startOverlayTimer(timeout: 3)
            statusViewController.escalateFeedback(for: camera.trackingState, inSeconds: 3.0)
        case .normal:
            stopOverlayTimer()
            if coachingOverlay.isActive {
                coachingOverlay.setActive(false, animated: true)
            }
            statusViewController.cancelScheduledMessage(for: .trackingStateEscalation)
            showVirtualContent()
        }
    }

    @objc private func coachingOverlayTime() {
        stopOverlayTimer()
        coachingOverlay.setActive(true, animated: true)
    }

    private func startOverlayTimer(timeout: TimeInterval) {
        if let valid = overlayTimer?.isValid, valid {
            return // other timer is still valid, let him trigger the action
        }
        overlayTimer = Timer.scheduledTimer(timeInterval: timeout,
                                        target: self,
                                        selector: #selector(coachingOverlayTime),
                                        userInfo: nil,
                                        repeats: false)
    }

    private func stopOverlayTimer() {
        if let valid = overlayTimer?.isValid, valid {
            overlayTimer?.invalidate()
            overlayTimer = nil
        }
    }

    func showVirtualContent() {
        makeButtons(hidden: false)
        vm.makeAll(hidden: false)
    }

    func session(_ session: ARSession, didFailWithError error: Error) {
        guard error is ARError else { return }

        let errorWithInfo = error as NSError
        let messages = [
            errorWithInfo.localizedDescription,
            errorWithInfo.localizedFailureReason,
            errorWithInfo.localizedRecoverySuggestion
        ]

        let errorMessage = messages.compactMap({ $0 }).joined(separator: "\n")

        DispatchQueue.main.async { [weak self] in
            self?.displayErrorMessage(
                title: "The AR session failed".localized,
                message: errorMessage
            )
        }
    }

    func sessionWasInterrupted(_ session: ARSession) {
        // Hide content before going into the background.
        hideVirtualContent()
    }

    internal func makeButtons(hidden: Bool) {
        [
            btnAdd,
            btnOK,
            btnDelete,
            imgBtnDelete,
            btnStairs,
            imgBtnStairs,
            btnShowHidePlanes,
        ].forEach({ $0?.isHidden = hidden })
    }

    /// - Tag: HideVirtualContent
    func hideVirtualContent() {
        makeButtons(hidden: true)
        vm.makeAll(hidden: true)
    }

    /*
     Allow the session to attempt to resume after an interruption.
     This process may not succeed, so the app must be prepared
     to reset the session if the relocalizing status continues
     for a long time -- see `escalateFeedback` in `StatusViewController`.
     */
    /// - Tag: Relocalization
    func sessionShouldAttemptRelocalization(_ session: ARSession) -> Bool {
        return true
    }
}
