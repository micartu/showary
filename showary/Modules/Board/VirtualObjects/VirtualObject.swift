//
//  VirtualObject.swift
//  showary
//
//  Created by Michael Artuerhof on 01.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation
import SceneKit
import ARKit

class VirtualObject: SCNReferenceNode {
    /// The alignments that are allowed for a virtual object.
    var allowedAlignment: ARRaycastQuery.TargetAlignment = .any

    /// The object's corresponding ARAnchor.
    var anchor: ARAnchor?

    /// object's name to be shown to user
    var caption: String?
}
