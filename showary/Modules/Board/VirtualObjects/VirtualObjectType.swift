//
//  VirtualObjectType.swift
//  showary
//
//  Created by Michael Artuerhof on 20.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

enum VirtualObjectType: Int {
    case sel = 0b0001
    case badge = 0b0010
    case plane = 0b0100
    case obj = 0b1000

    static var planes: Int {
        return VirtualObjectType.plane.rawValue
    }

    static var visibleMask: Int {
        let types: [VirtualObjectType] = [
            .plane,
            .obj
        ]
        return types.reduce(0) { $0 | $1.rawValue }
    }
}
