//
//  DissolveTransition.swift
//  showary
//
//  Created by Michael Artuerhof on 26/08/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

final class DissolveTransition: NSObject, UIViewControllerAnimatedTransitioning {
    let kAniTime: TimeInterval = 0.5

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return kAniTime
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to) else {
                transitionContext.completeTransition(false)
                return
        }
        let containerView = transitionContext.containerView
        containerView.addSubview(toVC.view)
        containerView.addSubview(fromVC.view)
        guard let logoView = fromVC.view.viewWithTag(const.logo)
            else { fatalError("Please set tag for LOGO!") }
        UIView.animate(withDuration: kAniTime,
                       delay: 0.0,
                       options: [.curveEaseIn],
                       animations: {
                        let s: CGFloat = 10
                        logoView.transform = CGAffineTransform(scaleX: s, y: s)
                        logoView.alpha = 0
                        fromVC.view.alpha = 0
        }, completion: { _ in
            fromVC.view.removeFromSuperview()
            transitionContext.completeTransition(true)
        })
    }

    private struct const {
        static let logo = 111
    }
}
