//
//  AppCoordinator.swift
//  showary
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

final class AppCoordinator: BaseCoordinator {
    init() {
        super.init(parent: nil)
    }

    func launchFrom(_ window: UIWindow) {
        start()
        window.rootViewController = cntr?.vc
    }

    override func start() {
        // main content (first show a splash):
        let s = SplashCoordinator(parent: self)
        // initialize vc:
        coordinate(to: s)
        // wrap it into a navigation controller
        let n = UINavigationController(rootViewController: s.cntr!.vc)
        navigationController = n
        cntr = n
    }

    override func exitFrom(coordinator: BaseCoordinator, with result: Any?) {
        super.exitFrom(coordinator: coordinator, with: result)
        if result == nil {
            let next: BaseCoordinator
            if coordinator is SplashCoordinator {
                next = BoardCoordinator(parent: self)
            } else if coordinator is LoginCoordinator {
                next = BoardCoordinator(parent: self)
            } else { // WTF?
                next = LoginCoordinator(parent: self)
            }
            move(to: next)
        } else if result is LifeCycleExit {
            return // life cycle events must be handled somewhere else
        } else {
            print("!AppCoordinator: unhandled result: \(String(describing: result))")
        }
        // clean old view controllers but after some delay
        // otherwise we'll break the transition animation :(
        delay(0.5) { [weak self] in
            self?.cleanControllersStack()
        }
    }

    // MARK: - BaseCoordinator

    override func presented(on vc: ControllerProtocol, controller: ControllerProtocol?) {
        if let nvc = controller {
            navigationController?.push(nvc, animated: true)
        }
    }

    // MARK: - Private / Internal

    internal func move(to coordinator: BaseCoordinator) {
        stopChildren()
        ccoordinator = coordinator
        coordinate(to: coordinator)
    }

    internal func cleanControllersStack() {
        if let nav = navigationController as? UINavigationController {
            // we manage only one controller opened at a time:
            if let last = nav.viewControllers.last {
                nav.setViewControllers([last], animated: false)
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    internal weak var ccoordinator: BaseCoordinator? = nil
}
