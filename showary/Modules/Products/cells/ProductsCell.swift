//
//  ProductsCell.swift
//  showary
//
//  Created by Michael Artuerhof on 10/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ProductsCellDelegate: class, TouchableCell {
}

final class ProductsCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(cellTapped))
        addGestureRecognizer(tap)
    }

    @objc private func cellTapped () {
        delegate?.cellTouched(id: id)
    }

    var id = ""
    weak var delegate: ProductsCellDelegate? = nil
}
