//
//  ProductsViewModel.swift
//  showary
//
//  Created by Michael Artuerhof on 10/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ProductsDispatcherProtocol: class {
    func callProductInfoWith(_ id: String, for store: Store?)
    func callSavedModels()
    func callScanner()
    func make(session: ARSessionControl)
}

protocol ProductsViewModelProtocol: CommonViewModel,
    ViewReadinessProtocol,
    ViewAppearanceProtocol {
}

final class ProductsViewModel: MultipleSectionPresenter {
	weak var dispatcher: (AlertProtocol & ProductsDispatcherProtocol)?
	weak var view: ProductsViewProtocol?

    fileprivate let net: NetProductsService
    fileprivate let secret: SecretService
    fileprivate let imgLoader: ImageLoaderService
    fileprivate let cache: CacheUserService
    fileprivate var stores = [Store]()
    fileprivate var sessionPaused = false
    fileprivate var storeIdItems = [String:[String]]() // store id <-> item ids

	init(net: NetProductsService,
         secret: SecretService,
         imgLoader: ImageLoaderService,
         cache: CacheUserService) {
        self.net = net
        self.secret = secret
        self.imgLoader = imgLoader
        self.cache = cache
		super.init(titles: [])
	}

    override func getContents() {
        dispatcher?.showBusyIndicator()
        checkCurrentUsrExistance(secret: secret, cache: cache) { [weak self] _ in
            self?.net.getStoreList(
                success: { list in
                    self?.dispatcher?.hideBusyIndicator()
                    self?.titles.removeAll()
                    self?.stores = list
                    self?.loadSections()
                }
                , failure: { [weak self] e in
                    self?.dispatcher?.hideBusyIndicator()
                    self?.dispatcher?.show(error: e.localizedDescription)
                }
            )
        }
    }

    fileprivate func loadSections() {
        for i in 0..<stores.count {
            models[i] = [CellAnyModel]()
            loadModels(with: theme, section: i)
        }
        for s in stores {
            titles.append(s.name)
        }
    }
}

// MARK: - SingleSectionInteractorInput
extension ProductsViewModel: MultipleSectionInteractorInput {
    func loadModels(with theme: Theme, section: Int) {
        let sid = stores[section].id
        net.getItemListForStore(
            name: sid,
            success: { [weak self] items in
                let mm = items.map({
                    ProductsModel(theme: theme,
                                  id: $0.id,
                                  title: $0.name,
                                  descr: $0.descr,
                                  image: $0.img,
                                  imgLoader: self?.imgLoader,
                                  delegate: self)
                })
                self?.storeIdItems[sid] = items.map { $0.id }
                self?.fetched(models: mm, section: section)
        }, failure: { [weak self] e in
            self?.dispatcher?.hideBusyIndicator()
            self?.dispatcher?.show(error: e.localizedDescription)
            }
        )
	}
}

// ProductsCellDelegate

// MARK: - ProductsCellDelegate
extension  ProductsViewModel: ProductsCellDelegate {
    func cellTouched(id: String) {
        var sid = ""
        for (s, vals) in storeIdItems {
            if vals.contains(id) {
                sid = s
                break
            }
        }
        if let store = stores.filter({ $0.id == sid }).first {
            dispatcher?.callProductInfoWith(id, for: store)
        }
    }
}

// MARK: - ProductsViewModelProtocol
extension  ProductsViewModel: ProductsViewModelProtocol {
    func viewIsReady() {
        let img = UIImage(named: "saved")?
            .imageWith(newSize: CGSize(width: 22, height: 22))
        view?.installRightButton(icon: img) { [weak self] in
            self?.dispatcher?.callSavedModels()
        }
        let q = UIImage(named: "qr")?
            .imageWith(newSize: CGSize(width: 22, height: 22))
        view?.installSecondRightButton(icon: q) { [weak self] in
            self?.dispatcher?.make(session: .pause)
            self?.sessionPaused = true
            self?.dispatcher?.callScanner()
        }
    }

    func viewWillAppear() {
        dispatcher?.hideBusyIndicator()
        if sessionPaused {
            sessionPaused = false
            dispatcher?.make(session: .resume)
        }
    }
}
