//
//  ProductsCoordinator.swift
//  showary
//
//  Created by Michael Artuerhof on 10/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class ProductsCoordinator: BaseCoordinator {
    override func start() {
        let vc = ProductsViewController.create()
        vc.title = "Products".localized
        vc.initializer = { tv in
            let identifier = String(describing: ProductsCell.self)
            tv.contentInset = UIEdgeInsets(top: 20, left: 0,
                                           bottom: 0, right: 0)
            tv.register(UINib(nibName: identifier, bundle: nil),
                        forCellReuseIdentifier: identifier)
        }
        let viewModel = ProductsViewModel(net: ServicesAssembler.inject(),
                                          secret: ServicesAssembler.inject(),
                                          imgLoader: ServicesAssembler.inject(),
                                          cache: ServicesAssembler.inject())
        viewModel.dispatcher = self
        viewModel.source = viewModel
        vc.viewModel = viewModel
        viewModel.view = vc
        cntr = vc
    }
}

// MARK: - ProductsDispatcherProtocol
extension  ProductsCoordinator: ProductsDispatcherProtocol {
    func callProductInfoWith(_ id: String, for store: Store?) {
        let c = ProductInfoCoordinator(parent: self, store: store, id: id)
        coordinate(to: c)
    }

    func callSavedModels() {
        let c = AvailModelsCoordinator(parent: parent, output: self)
        coordinate(to: c)
    }

    func callScanner() {
        let c = QScannerCoordinator(parent: self)
        coordinate(to: c)
    }
}

// MARK: - AvailModelsOutput
extension ProductsCoordinator: AvailModelsOutput {
    func selected(model: Model) {
        broadcast(result: model.id)
    }

    func showListOfModels() {
    }

    func make(session: ARSessionControl) {
        broadcast(result: session)
    }
}
