//
//  ProductsModel.swift
//  showary
//
//  Created by Michael Artuerhof on 10/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ProductsCellModel: CommonCell { }

struct ProductsModel: ProductsCellModel {
    let theme: Theme
    let id: String
    let title: String
    let descr: String
    let image: String
	weak var imgLoader: ImageLoaderService?
    weak var delegate: ProductsCellDelegate?

	func setup(cell: ProductsCell) {
        cell.id = id
        cell.delegate = delegate

        cell.lblTitle.text = title
        cell.lblTitle.font = theme.boldFont.withSize(theme.sz.middle1)
        cell.lblTitle.textColor = theme.mainTextColor

        cell.lblDescription.text = descr
        cell.lblDescription.font = theme.regularFont.withSize(theme.sz.middle1)
        cell.lblDescription.textColor = theme.mainTextColor

        if image.count > 0 {
            if let av = imgLoader?.loadOffline(image: image) {
                cell.imgView.image = av
            } else {
                imgLoader?.load(image: image) { [weak cell] im in
                    if cell?.id == self.id {
                        cell?.imgView.image = im
                    }
                }
            }
        } else {
            cell.imgView.image = UIImage(named: "debug")
        }
    }
}
