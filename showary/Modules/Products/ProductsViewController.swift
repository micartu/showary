//
//  ProductsViewController.swift
//  showary
//
//  Created by Michael Artuerhof on 10/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ProductsViewProtocol: class {
    func installRightButton(icon: UIImage?, handler: @escaping SimpleHandler)
    func installSecondRightButton(icon: UIImage?, handler: @escaping SimpleHandler)
}

final class ProductsViewController: BaseTableViewController {
    var vm: ProductsViewModelProtocol {
        get {
            viewModel as! ProductsViewModelProtocol
        }
    }
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.viewIsReady()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vm.viewWillAppear()
    }

    @objc private func rightButtonAction() {
        rightButtonHandlers[0]()
    }

    @objc private func secondRightButtonAction() {
        rightButtonHandlers[1]()
    }

    private var rightButtonHandlers = [SimpleHandler]()
}

// MARK: - ProductsViewProtocol
extension ProductsViewController: ProductsViewProtocol {
    func installRightButton(icon: UIImage?, handler: @escaping SimpleHandler) {
        let r = UIBarButtonItem(image: icon,
                                style: .plain,
                                target: self,
                                action: #selector(rightButtonAction))
        r.tintColor = theme.tintColor
        navigationItem.rightBarButtonItem = r
        rightButtonHandlers.append(handler)
    }

    func installSecondRightButton(icon: UIImage?, handler: @escaping SimpleHandler) {
        if let q = navigationItem.rightBarButtonItem {
            let r = UIBarButtonItem(image: icon,
                                    style: .plain,
                                    target: self,
                                    action: #selector(secondRightButtonAction))
            r.tintColor = theme.tintColor
            navigationItem.rightBarButtonItems = [r, q]
            rightButtonHandlers.append(handler)
        } else {
            fatalError("cannot assign second right button without the first one")
        }
    }
}

// MARK: - ViewControllerable
extension ProductsViewController: ViewControllerable {
	static var storyboardName: String {
		return "Products"
	}
}
