//
//  ButtonizedModel.swift
//  showary
//
//  Created by Michael Artuerhof on 28/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ButtonizedCellModel: CommonCell { }

enum ButtonizedType {
    case regular
    case destruct
}

struct ButtonizedModel: ButtonizedCellModel {
    let theme: Theme
    let id: String
    var enabled: Bool
    var type: ButtonizedType
    let buttonCaption: String
	weak var delegate: ButtonizedCellDelegate?

	func setup(cell: ButtonizedCell) {
        cell.id = id
        cell.delegate = delegate

        cell.lblTitle.text = ""

        cell.btnAction.setTitleColor(theme.mainBackColor, for: .normal)
        if enabled {
            let c: UIColor
            switch type {
            case .regular:
                c = theme.tintColor
            case .destruct:
                c = theme.warnColor
            }
            cell.btnAction.foregroundColor = c
        } else {
            cell.btnAction.foregroundColor = theme.inactiveColor
        }
        cell.btnAction.setTitle(buttonCaption, for: .normal)
        cell.btnAction.isEnabled = enabled
    }
}
