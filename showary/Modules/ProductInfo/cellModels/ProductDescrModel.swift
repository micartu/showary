//
//  ProductDescrModel.swift
//  showary
//
//  Created by Michael Artuerhof on 13/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ProductDescrCellModel: CommonCell { }

struct ProductDescrModel: ProductDescrCellModel {
    let theme: Theme
    let id: String
    let content: String
	weak var delegate: ProductDescrCellDelegate?

	func setup(cell: ProductDescrCell) {
        cell.id = id
        cell.delegate = delegate
        
        cell.lblContent.text = content
        cell.lblContent.font = theme.regularFont.withSize(theme.sz.middle2)
        cell.lblContent.textColor = theme.mainTextColor
    }
}
