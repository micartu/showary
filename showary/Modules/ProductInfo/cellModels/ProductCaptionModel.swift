//
//  ProductCaptionModel.swift
//  showary
//
//  Created by Michael Artuerhof on 13/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ProductCaptionCellModel: CommonCell { }

struct ProductCaptionModel: ProductCaptionCellModel {
    let theme: Theme
    let id: String
    let title: String
    let subtitle: String
    let image: String
	weak var delegate: ProductCaptionCellDelegate?
    weak var imgLoader: ImageLoaderService?

	func setup(cell: ProductCaptionCell) {
        cell.id = id
        cell.delegate = delegate

        cell.lblTitle.text = title
        cell.lblTitle.font = theme.boldFont.withSize(theme.sz.middle1)
        cell.lblTitle.textColor = theme.mainTextColor

        cell.lblSubtitle.text = subtitle
        cell.lblSubtitle.font = theme.regularFont.withSize(theme.sz.middle1)
        cell.lblSubtitle.textColor = theme.mainTextColor
    }
}
