//
//  ProductInfoCoordinator.swift
//  showary
//
//  Created by Michael Artuerhof on 13/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class ProductInfoCoordinator: AuthorizableBaseCoordinator {
    init(parent: BaseCoordinator?, store: Store?, id: String) {
        self.id = id
        self.store = store
        super.init(secret: ServicesAssembler.inject(),
                   parent: parent)
    }

	override func start() {
		let vc = ProductInfoViewController.create()
		vc.title = "ProductInfo".localized
        vc.addRefreshControl = false
		vc.initializer = { tv in
            let ids = [
                ProductCaptionCell.self,
                BigLogoCell.self,
                ProductDescrCell.self,
                ButtonizedCell.self,
            ]
            for id in ids {
                let identifier = String(describing: id)
                tv.register(UINib(nibName: identifier, bundle: nil),
                            forCellReuseIdentifier: identifier)
            }
            tv.separatorStyle = .none
            //tv.isUserInteractionEnabled = false
		}
        let viewModel = ProductInfoViewModel(id: id,
                                             store: store,
                                             net: ServicesAssembler.inject(),
                                             mdlLoader: ServicesAssembler.inject(),
                                             cache: ServicesAssembler.inject(),
                                             imgLoader: ServicesAssembler.inject())
        viewModel.dispatcher = self
        viewModel.source = viewModel
        vc.vm = viewModel
        viewModel.view = vc
        cntr = vc
	}

    fileprivate let store: Store?
    fileprivate let id: String
}

// MARK: - ProductInfoDispatcherProtocol
extension  ProductInfoCoordinator: ProductInfoDispatcherProtocol {
    func show(model: String) {
        broadcast(result: model)
    }

    func showInfoAboutStoreWith(id: String, loaded store: Store?) {
        let si = FirmInfoCoordinator(parent: self, id: id, store: store)
        coordinate(to: si)
    }

    func actionIfAuthorized(completion: @escaping SimpleHandler) {
        checkAuthorizationAndExecute(completion: completion)
    }
}
