//
//  ProductInfoViewModel.swift
//  showary
//
//  Created by Michael Artuerhof on 13/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol ProductInfoDispatcherProtocol: class {
    func show(model: String)
    func showInfoAboutStoreWith(id: String, loaded store: Store?)
    func actionIfAuthorized(completion: @escaping SimpleHandler)
}

protocol ProductInfoViewModelProtocol:
    ViewReadinessProtocol {
    func showModelPressed()
}

final class ProductInfoViewModel: SingleSectionPresenter {
	weak var dispatcher: (AlertProtocol & ProductInfoDispatcherProtocol)?
	weak var view: ProductInfoViewProtocol?

	init(id: String,
         store: Store?,
         net: NetProductService,
         mdlLoader: ModelLoaderService,
         cache: CacheModelService,
         imgLoader: ImageLoaderService) {
        self.id = id
        self.store = store
        self.net = net
        self.mdlLoader = mdlLoader
        self.cache = cache
        self.imgLoader = imgLoader
		super.init()
	}

    fileprivate let id: String
    fileprivate let net: NetProductService
    fileprivate let mdlLoader: ModelLoaderService
    fileprivate let cache: CacheModelService
    fileprivate let imgLoader: ImageLoaderService
    fileprivate let store: Store?
    fileprivate var item: StoreItem?

    fileprivate struct c {
        static let delete = "delete"
        static let buy = "buy"
    }
}

// MARK: - SingleSectionInteractorInput
extension ProductInfoViewModel: SingleSectionInteractorInput {
	func loadModels(with theme: Theme) {
        dispatcher?.showBusyIndicator()
        net.getItemWithID(
            id,
            success: { [weak self] item in
                guard let `self` = self else { return }
                self.dispatcher?.hideBusyIndicator()
                let f = self.store?.name ?? "?"
                let t = ProductCaptionModel(theme: theme,
                                            id: "caption",
                                            title: f,
                                            subtitle: item.name + "\n" + item.descr,
                                            image: item.img,
                                            delegate: self,
                                            imgLoader: self.imgLoader)
                let d = ProductDescrModel(theme: theme,
                                          id: "descr",
                                          content: item.descr,
                                          delegate: self)
                let l = BigLogoModel(
                    theme: theme,
                    id: "",
                    image: item.img,
                    delegate: self,
                    imgLoader: self.imgLoader
                )
                let buy = ButtonizedModel(theme: theme,
                                          id: c.buy,
                                          enabled: true,
                                          type: .regular,
                                          buttonCaption: "Buy".localized,
                                          delegate: self)
                let itms: [CellAnyModel]
                if self.cache.modelExistsWith(id: item.id) {
                    let del = ButtonizedModel(theme: theme,
                                              id: c.delete,
                                              enabled: true,
                                              type: .destruct,
                                              buttonCaption: "Delete".localized,
                                              delegate: self)
                    itms = [t, l, d, buy, del]
                } else {
                    itms = [t, l, buy, d]
                }
                self.item = item
                self.fetched(models: itms)
                self.view?.makeARButton(hidden: false, animated: true)
            },
            failure: { [weak self] e in
                self?.dispatcher?.hideBusyIndicator()
                self?.dispatcher?.show(error: e.localizedDescription)
            }
        )
	}
}

// MARK: - ProductCaptionCellDelegate
extension  ProductInfoViewModel: ProductCaptionCellDelegate {
    fileprivate func downloadButton(enabled: Bool) {
        view?.makeARButton(hidden: !enabled, animated: true)
    }

    func cellTouched(id: String) {
        if id == c.delete {
            if let i = item {
                dispatcher?.showYesNO(
                    title: "Are you sure?".localized,
                    message: "The item will be deleted from disk".localized,
                    actionYes: { [weak self] _ in
                        self?.cache.delete(modelIDs: [i]) {
                            self?.mdlLoader.deleteModel(named: i.model) {
                                self?.refreshContents()
                            }
                        }
                    }, actionNo: nil
                )
            }
        } else if id == c.buy {
            dispatcher?.actionIfAuthorized { [weak self] in
                self?.dispatcher?.show(
                    title: "Authorized",
                    message: "You can buy me now!"
                )
            }
        }
    }

    func showInfoAboutFirmOfItemWith(id: String) {
        dispatcher?.showInfoAboutStoreWith(id: self.id, loaded: store)
    }
}

// MARK: - ButtonizedCellDelegate
extension  ProductInfoViewModel: ButtonizedCellDelegate {
}

// MARK: - ProductDescrCellDelegate
extension  ProductInfoViewModel: ProductDescrCellDelegate {
}

// MARK: - ProductInfoViewModelProtocol
extension  ProductInfoViewModel: ProductInfoViewModelProtocol {
    func viewIsReady() {
        view?.makeARButton(hidden: true, animated: false)
    }

    func showModelPressed() {
        if let i = item {
            let opt = [String:String](fromJSONString: nil)
            downloadButton(enabled: false)
            if !cache.modelExistsWith(id: i.id) { // load model from network
                dispatcher?.showBusyIndicator()
                mdlLoader.loadModel(
                    named: i.model,
                    options: opt,
                    completion: { [weak self] (mdl, err) in
                        self?.downloadButton(enabled: true)
                        if let e = err {
                            self?.dispatcher?.hideBusyIndicator()
                            self?.dispatcher?.show(error: e.localizedDescription)
                        } else {
                            let m = Model(id: i.id,
                                          img: i.img,
                                          path: i.model,
                                          descr: i.data)
                            self?.cache.create(models: [m]) {
                                self?.dispatcher?.hideBusyIndicator()
                                self?.dispatcher?.show(model: i.id)
                            }
                        }
                    }
                )
            } else { // load model from disk
                dispatcher?.show(model: i.id)
            }
        }
    }
}

// MARK: - BigLogoCellDelegate
extension  ProductInfoViewModel: BigLogoCellDelegate {
    func update(id: String) {
        runOnMainThread { [weak self] in
            if let (_, i) = self?.findModelWith(id: id) {
                if i > -1 {
                    let upd = IIndexPathCount(path: IndexPath(row: i, section: 0),
                                              count: 1,
                                              animated: true)
                    self?.updateItems.set(upd)
                }
            }
        }
    }
}
