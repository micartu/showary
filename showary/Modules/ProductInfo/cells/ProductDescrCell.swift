//
//  ProductDescrCell.swift
//  showary
//
//  Created by Michael Artuerhof on 13/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ProductDescrCellDelegate: class {
}

final class ProductDescrCell: UITableViewCell {
    @IBOutlet weak var lblContent: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    var id = ""
    weak var delegate: ProductDescrCellDelegate? = nil
}
