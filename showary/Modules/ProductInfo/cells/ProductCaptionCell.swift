//
//  ProductCaptionCell.swift
//  showary
//
//  Created by Michael Artuerhof on 13/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ProductCaptionCellDelegate: class {
    func showInfoAboutFirmOfItemWith(id: String)
}

final class ProductCaptionCell: UITableViewCell {
    @IBOutlet weak var imInfo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction
    @objc func cellTapped (_ sender: UIButton) {
        delegate?.showInfoAboutFirmOfItemWith(id: id)
    }

    var id = ""
    weak var delegate: ProductCaptionCellDelegate? = nil
}
