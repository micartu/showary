//
//  ButtonizedCell.swift
//  showary
//
//  Created by Michael Artuerhof on 28/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ButtonizedCellDelegate: class, TouchableCell {
}


final class ButtonizedCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnAction: BorderedButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        btnAction.addTarget(self,
                            action: #selector(cellTapped),
                            for: .touchUpInside)
    }

    @objc private func cellTapped () {
        delegate?.cellTouched(id: id)
    }

    var id = ""
    weak var delegate: ButtonizedCellDelegate? = nil
}
