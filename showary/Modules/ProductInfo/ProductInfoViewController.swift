//
//  ProductInfoViewController.swift
//  showary
//
//  Created by Michael Artuerhof on 13/02/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ProductInfoViewProtocol: class {
    func makeARButton(hidden: Bool,
                      animated: Bool,
                      completion: @escaping (() -> Void))
}

final class ProductInfoViewController: BaseTableViewController {
    @IBOutlet weak var viewDownload: UIView!
    @IBOutlet weak var btnDownload: UIButton!
    var vm: ProductInfoViewModelProtocol! {
        didSet {
            viewModel = vm as? CommonViewModel
        }
    }

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.viewIsReady()
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        viewDownload.layer.cornerRadius = viewDownload.bounds.size.width / 2
        viewDownload.backgroundColor = theme.tintColor
        btnDownload.tintColor = theme.mainBackColor
    }

    @IBAction @objc func downloadAction(_ sender: UIButton) {
        vm.showModelPressed()
    }
}

// MARK: - ProductInfoViewProtocol
extension ProductInfoViewController: ProductInfoViewProtocol {
    func makeARButton(hidden: Bool,
                      animated: Bool,
                      completion: @escaping (() -> Void)) {
        func makeIT() {
            if hidden {
                let sz = UIScreen.main.bounds.size
                viewDownload.transform = CGAffineTransform(translationX: sz.width * 1.5, y: 0)
            } else {
                viewDownload.transform = .identity
            }
        }
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                makeIT()
            }, completion: { _ in
                completion()
            })
        } else {
            makeIT()
            completion()
        }
    }
}

extension ProductInfoViewProtocol {
    func makeARButton(hidden: Bool,
                      animated: Bool) {
        makeARButton(hidden: hidden, animated: animated, completion: {})
    }
}

// MARK: - ViewControllerable
extension ProductInfoViewController: ViewControllerable {
	static var storyboardName: String {
		return "ProductInfo"
	}
}
