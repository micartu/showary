//
//  AvailModelsCell.swift
//  showary
//
//  Created by Michael Artuerhof on 01/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol AvailModelsCellDelegate: class, TouchableCell {
    func showInfoFor(id: String)
}

final class AvailModelsCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vibrancyView: UIVisualEffectView!
    @IBOutlet weak var infoImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(cellTapped))
        addGestureRecognizer(tap)
    }

    @objc private func cellTapped () {
        delegate?.cellTouched(id: id)
    }

    @objc @IBAction func infoTapped (_ send: UIButton) {
        delegate?.showInfoFor(id: id)
    }

    var id = ""
    weak var delegate: AvailModelsCellDelegate? = nil
}
