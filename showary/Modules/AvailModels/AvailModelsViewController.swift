//
//  AvailModelsViewController.swift
//  showary
//
//  Created by Michael Artuerhof on 01/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol AvailModelsViewProtocol: class {
}

final class AvailModelsViewController: BaseTableViewController {
    @IBOutlet weak var imDownload: UIImageView!
    @IBOutlet weak var btnDownload: UIButton!
    var vm: AvailModelsViewModelProtocol!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnDownload.addTarget(self, action: #selector(action(_:)), for: .touchUpInside)
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        btnDownload.tintColor = theme.mainTextColor
        imDownload.tintColor = theme.tintColor
    }

    @objc private func action(_ sender: UIView) {
        let anim = CABasicAnimation(keyPath: "transform.scale")
        let kD: Double = 0.25
        anim.fromValue = 1
        anim.toValue = 0.5
        anim.duration = kD
        imDownload.layer.add(anim, forKey: nil)
        delay(kD) { [weak self] in
            self?.vm.downloadTouched()
        }
    }
}

// MARK: - AvailModelsViewProtocol
extension AvailModelsViewController: AvailModelsViewProtocol {
}

// MARK: - ViewControllerable
extension AvailModelsViewController: ViewControllerable {
	static var storyboardName: String {
		return "AvailModels"
	}
}
