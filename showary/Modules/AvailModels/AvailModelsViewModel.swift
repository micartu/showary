//
//  AvailModelsViewModel.swift
//  showary
//
//  Created by Michael Artuerhof on 01/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol AvailModelsDispatcherProtocol: class, ViewDismissibleProtocol {
    func showInfoFor(id: String)
}

protocol AvailModelsViewModelProtocol {
    func downloadTouched()
}

final class AvailModelsViewModel: FetchedDataPresenter {
	weak var dispatcher: (AlertProtocol & AvailModelsDispatcherProtocol)?
	weak var view: AvailModelsViewProtocol?
    fileprivate var imgLoader: ImageLoaderService
    weak var output: AvailModelsOutput?
    fileprivate let cache: CacheModelService
    fileprivate var alignment = ""
    private var state = [String: ModelCellState]()
    private struct ModelCellState {
        let index: IndexPath
        let m: Model
    }

    init(manager: FetchManagerProtocol,
         cache: CacheModelService,
         imgLoader: ImageLoaderService) {
        self.cache = cache
        self.imgLoader = imgLoader
        super.init(manager: manager)
    }
}

// MARK: - FetchedDataInteractorInput
extension AvailModelsViewModel: FetchedDataInteractorInput {
    fileprivate func dict(from json: String) -> [String:String] {
        if let d = [String:String](fromJSONString: json) {
            return d
        }
        return [:]
    }

	func wrap(entity: Any,
              indexPath: IndexPath,
              with theme: Theme) -> CellAnyModel {
        if let i = entity as? Model {
            let id = "\(i.id)"
            let d = dict(from: i.descr)
            let title = d["title"] ?? "model: #\(id)"
            let isAvailableNow = ((d[Const.ar.alignment] ?? "") == alignment)
            state[id] = ModelCellState(
                index: indexPath,
                m: i
            )
            return AvailModelsModel(
                theme: theme,
                id: id,
                title: title,
                image: i.img,
                shown: isAvailableNow,
                imgLoader: imgLoader,
                delegate: self
            )
        }
        fatalError("AvailModelsViewModel: Asked about an unknown entity: \(entity)")
    }
}

// MARK: - AvailModelsViewModelProtocol
extension AvailModelsViewModel: AvailModelsViewModelProtocol {
    func set(alignment: String) {
        self.alignment = alignment
    }

    func downloadTouched() {
        output?.showListOfModels()
    }
}

// MARK: - AvailModelsCellDelegate
extension AvailModelsViewModel: AvailModelsCellDelegate {
    func cellTouched(id: String) {
        if let m = cache.modelWith(id: id) {
            output?.selected(model: m)
            dispatcher?.dismiss()
        }
    }

    func showInfoFor(id: String) {
        dispatcher?.showInfoFor(id: id)
    }
}

// MARK: - CellOperationProtocol
extension AvailModelsViewModel: CellOperationProtocol {
    func cellOperation(for indexPath: IndexPath) -> [CellOperation] {
        return [.delete]
    }

    func deleteCell(at indexPath: IndexPath) {
        for (_, v) in state {
            if v.index == indexPath {
                cache.delete(modelIDs: [v.m]) { [weak self] in
                    self?.updateData.set(true)
                }
            }
        }
    }
}
