//
//  AvailModelsCoordinator.swift
//  showary
//
//  Created by Michael Artuerhof on 01/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class AvailModelsCoordinator: BaseCoordinator {
    init(parent: BaseCoordinator?, output: AvailModelsOutput? = nil) {
        if output == nil {
            if let p = parent?.cntr?.vc as? AvailModelsOutput {
                self.output = p
            }
        } else {
            self.output = output
        }
        super.init(parent: parent)
    }

	override func start() {
		let vc = AvailModelsViewController.create()
        vc.addRefreshControl = false
        vc.title = "AvailModels".localized
		vc.initializer = { tv in
            tv.separatorStyle = .none
            //tv.separatorEffect = UIVibrancyEffect(blurEffect: UIBlurEffect(style: .light))
            let identifier = String(describing: AvailModelsCell.self)
			tv.register(UINib(nibName: identifier, bundle: nil),
				        forCellReuseIdentifier: identifier)
		}
        checkCurrentUsrExistance(
            secret: ServicesAssembler.inject(),
            cache: ServicesAssembler.inject()
        ) { [weak self] u in
            let cf: CacheFetchRequestsService = ServicesAssembler.inject()
            let viewModel = AvailModelsViewModel(
                manager: cf.fetchDataControllerForModelsForUser(login: u.login),
                cache: ServicesAssembler.inject(),
                imgLoader: ServicesAssembler.inject()
            )
            viewModel.dispatcher = self
            viewModel.source = viewModel
            vc.viewModel = viewModel
            vc.vm = viewModel
            viewModel.view = vc
            viewModel.output = self?.output
            self?.cntr = vc
        }
	}

    fileprivate weak var output: AvailModelsOutput?
}

// MARK: - AvailModelsDispatcherProtocol
extension AvailModelsCoordinator: AvailModelsDispatcherProtocol {
}

// MARK: - AvailModelsInput
extension AvailModelsCoordinator: AvailModelsInput {
    func dismiss() {
        if let nav = cntr?.vc.navigationController {
            if let f = nav.viewControllers.first {
                nav.setViewControllers([f], animated: true)
            }
        }
    }

    func reload() {
    }

    func showInfoFor(id: String) {
        dismiss()
    }
}
