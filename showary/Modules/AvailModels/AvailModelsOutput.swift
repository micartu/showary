//
//  AvailModelsOutput.swift
//  showary
//
//  Created by Michael Artuerhof on 04.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

protocol AvailModelsOutput: class {
    func selected(model: Model)
    func showListOfModels()
}
