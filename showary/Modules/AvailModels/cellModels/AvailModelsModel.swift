//
//  AvailModelsModel.swift
//  showary
//
//  Created by Michael Artuerhof on 01/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol AvailModelsCellModel: CommonCell { }

struct AvailModelsModel: AvailModelsCellModel {
    let theme: Theme
    let id: String
    let title: String
    let image: String
    let shown: Bool
    weak var imgLoader: ImageLoaderService?
	weak var delegate: AvailModelsCellDelegate?

	func setup(cell: AvailModelsCell) {
        cell.id = id
        cell.delegate = delegate

        cell.vibrancyView.alpha = shown ? 1.0 : 0.1

        cell.lblTitle.text = title
        cell.lblTitle.font = theme.boldFont.withSize(theme.sz.middle1)
        cell.lblTitle.textColor = theme.mainTextColor

        if image.count > 0 {
            if let av = imgLoader?.loadOffline(image: image) {
                cell.imgView.image = av
            } else {
                imgLoader?.load(image: image) { [weak cell] im in
                    if cell?.id == self.id {
                        cell?.imgView.image = im
                    }
                }
            }
        } else {
            cell.imgView.image = UIImage(named: "debug")
        }
    }
}
