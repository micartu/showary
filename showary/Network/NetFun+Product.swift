//
//  NetFun+Product.swift
//  showary
//
//  Created by Michael Artuerhof on 13.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension NetFun: NetProductService {
    func getItemWithID(_ id: String,
                       success: ((StoreItem) -> Void)?,
                       failure: ((NSError) -> Void)?) {
        getWithParams(url: "/item/\(id)/", params: nil, success: { (ans: NSDictionary) in
            let itm = parseStoreItem(from: ans)
            success?(itm)
        }, failure: { e in
            failure?(e)
        })
    }
}
