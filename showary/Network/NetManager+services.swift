//
//  NetManager+services.swift
//  showary
//
//  Created by Michael Artuerhof on 11.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

extension NetManager: ServiceLocatorModulProtocol {
    func registerServices(serviceLocator s: ServicesAssembler) {
        s.register(factory: { (named: String) in
            NetFun(netmanager: self, named: named) as NetAuthService
        })
        s.register(factory: { (named: String) in
            NetFun(netmanager: self, named: named) as NetProductsService
        })
        s.register(factory: { (named: String) in
            NetFun(netmanager: self, named: named) as NetProductService
        })
        s.register(factory: { (named: String) in
            NetFun(netmanager: self, named: named) as NetFirmService
        })
    }
}
