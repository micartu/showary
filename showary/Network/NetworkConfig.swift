//
//  NetworkConfig.swift
//  showary
//
//  Created by Michael Artuerhof on 26.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

struct NetworkConfig {
    var baseUrl: String
    var prefix: String
    var clientId: String
    var clientSecret: String

    init() {
        baseUrl = ""
        prefix = ""
        clientId = ""
        clientSecret = ""
    }

    static func getConfiguration(name: String) -> NetworkConfig {
        var out = NetworkConfig()
        guard let plistPath = Bundle.main.path(forResource: name, ofType: "plist") else {
            return out
        }
        let dict = NSDictionary(contentsOfFile: plistPath)!
        if let baseUrl = dict["server_base_url"] as? String {
            out.baseUrl = baseUrl
        }
        if let vprefix = dict["server_base_path"] as? String {
            out.prefix = vprefix
        }
        if let clientId = dict["client_id"] as? String {
            out.clientId = clientId
        }
        if let clientSecret = dict["client_secret"] as? String {
            out.clientSecret = clientSecret
        }
        return out
    }
}
