//
//  RegisterInfo.swift
//  Chatter
//
//  Created by Michael Artuerhof on 30/08/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

struct RegisterInfo: Codable {
    let type: String
    let scope: String
    let name: String
    let email: String
    let password: String
    let clientId: String
    let clientSecret: String

    enum CodingKeys: String, CodingKey {
        case type = "grant_type"
        case scope
        case name
        case email
        case password
        case clientId = "client_id"
        case clientSecret = "client_secret"
    }
}
