//
//  NetFun+Firm.swift
//  showary
//
//  Created by Michael Artuerhof on 11.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

extension NetFun: NetFirmService {
    func getStoreWithID(_ id: String,
                        success: ((Store) -> Void)?,
                        failure: ((NSError) -> Void)?) {
        getWithParams(url: "/store/\(id)/", params: nil, success: { (ans: NSDictionary) in
            let itm = parseStore(from: ans)
            success?(itm)
        }, failure: { e in
            failure?(e)
        })
    }
}
