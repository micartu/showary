//
//  NetQueueService.swift
//  showary
//
//  Created by Michael Artuerhof on 01/05/2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

protocol NetQueueService {
    func used(by: String)
    func removeAllOperations()
}

extension NetQueueService {
    func used(by: String) { }
}
