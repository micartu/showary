//
//  NetProductsService.swift
//  showary
//
//  Created by Michael Artuerhof on 10.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol NetProductsService: NetQueueService {
    func getStoreList(success: (([Store]) -> Void)?,
                      failure: ((NSError) -> Void)?)

    func getItemListForStore(name: String,
                             success: (([StoreItem]) -> Void)?,
                             failure: ((NSError) -> Void)?)
}
