//
//  NetFirmService.swift
//  showary
//
//  Created by Michael Artuerhof on 11.05.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import Foundation

protocol NetFirmService {
    func getStoreWithID(_ id: String,
                        success: ((Store) -> Void)?,
                        failure: ((NSError) -> Void)?)
}
