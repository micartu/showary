//
//  NetProductService.swift
//  showary
//
//  Created by Michael Artuerhof on 13.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol NetProductService: NetQueueService {
    func getItemWithID(_ id: String,
                       success: ((StoreItem) -> Void)?,
                       failure: ((NSError) -> Void)?)
}
