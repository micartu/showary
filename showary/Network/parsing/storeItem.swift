//
//  storeItem.swift
//  showary
//
//  Created by Michael Artuerhof on 10.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

func parseStoreItem(from d: NSDictionary) -> StoreItem {
    let iid = parseInt(d, key: "id")
    let id = "\(iid)"
    let name = parseString(d, for: "name")
    let descr = parseString(d, for: "description")
    let img = parseString(d, for: "image")
    let price = parseDouble(d, key: "price")
    let salePrice = parseDouble(d, key: "sale_price")
    let model = parseString(d, for: "model")
    let data: String
    if let d = d["data"] as? NSDictionary {
        data = d.toJSONString() ?? ""
    } else {
        data = parseString(d, for: "data")
    }
    return StoreItem(id: id,
                     name: name,
                     img: img,
                     descr: descr,
                     model: model,
                     data: data,
                     price: price,
                     salePrice: salePrice)
}
