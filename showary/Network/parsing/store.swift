//
//  store.swift
//  showary
//
//  Created by Michael Artuerhof on 10.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

func parseStore(from d: NSDictionary) -> Store {
    let iid = parseInt(d, key: "id")
    let id = "\(iid)"
    let name = parseString(d, for: "name")
    let descr = parseString(d, for: "description")
    let logo = parseString(d, for: "logo")
    let website = parseString(d, for: "website")
    let address = parseString(d, for: "address")
    return Store(id: id,
                 name: name,
                 descr: descr,
                 logo: logo,
                 addr: address,
                 website: website)
}
