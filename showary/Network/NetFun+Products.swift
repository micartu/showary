//
//  NetFun+Products.swift
//  showary
//
//  Created by Michael Artuerhof on 10.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension NetFun: NetProductsService {
    func getStoreList(success: (([Store]) -> Void)?,
                      failure: ((NSError) -> Void)?) {
        getWithParams(url: "/store/", params: nil, success: { (ans: [NSDictionary]) in
            let prds = ans.map({ parseStore(from: $0) })
            success?(prds)
        }, failure: { e in
            failure?(e)
        })
    }

    func getItemListForStore(name: String,
                             success: (([StoreItem]) -> Void)?,
                             failure: ((NSError) -> Void)?) {
        getWithParams(url: "/item/?store=\(name)", params: nil, success: { (ans: [NSDictionary]) in
            let prds = ans.map({ parseStoreItem(from: $0) })
            success?(prds)
        }, failure: { e in
            failure?(e)
        })
    }
}
