#!/usr/bin/env /usr/bin/python

import subprocess
import time
import sys

if __name__ == '__main__':
    i = 1
    ver = "- version bumped to"
    change_log_file = "- changed changelog"
    last_log = ""
    release = False
    if len(sys.argv) > 1 and sys.argv[1] == "--release":
        release = True

    while (True):
        cmd = "/usr/bin/git log -"  + str(i) + " --pretty=%B"
        log = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
        #print log
        if ver in log or change_log_file in log:
            break
        last_log = log
        i += 1

    # no commits, nothing to report, simply exit from script
    # for release it's possibile that we've got no commits,
    # it's published after testing of the DEBUG version is over
    if i == 1 and not release:
        sys.exit(1)

    # increment build number
    subprocess.call("./increment_build.sh")
    # read current version for report:
    cver = subprocess.Popen("xcodebuild -project showary.xcodeproj -showBuildSettings | awk '/MARKETING_VERSION/ { print $3 }'", shell=True, stdout=subprocess.PIPE).stdout.read().strip()
    vbuild = subprocess.Popen("xcrun agvtool vers -terse", shell=True, stdout=subprocess.PIPE).stdout.read().strip()
    build_str = ver + " " + cver + " b" + vbuild
    if release:
        build_str += " (RELEASE)"
    # commit that:
    subprocess.Popen("/usr/bin/git commit -a -m \"" +  build_str + "\"", shell=True)
    # wait a little bit for the commit to be finished:
    time.sleep(1)
    # write latest changes
    line = "-------------------------------------------------"
    # no changes available, it's a RELEASE version
    if i == 1:
        last_log = "(no functional changes)"
    last_log = "\n" + build_str + "\n" + line + "\n" + last_log
    with open('last_log','wb') as f:
        f.write("showary\n" + last_log)
    # write those changes into changelog:
    with open('ChangeLog','ab') as f:
        f.write(last_log)
    # commit ChangeLog too:
    subprocess.Popen("/usr/bin/git commit -a -m \"" + change_log_file + "\"", shell=True)
    # all ok
    sys.exit(0)
