//
//  CacheTests.swift
//  showaryTests
//
//  Created by Michael Artuerhof on 29.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import XCTest
@testable import showary

final class CacheTests: XCTestCase {
    private let secrets = MockSecrets()
    var cache: Cache!
    let kPhone = "12345"
    let kName = "john"
    let kMail = "john@mail.de"

    override func setUp() {
        cache = Cache(secret: secrets)
    }

    func testUserCreation() {
        let exp = self.expectation(description: "user creation")
        createUser { [unowned self] in
            XCTAssertTrue(self.cache.userExistsWith(login: self.kPhone))
            let u = self.cache.getUserWith(login: self.kPhone)
            XCTAssert(u?.name == self.kName)
            XCTAssert(u?.login == self.kPhone)
            self.cache.deleteUserWith(login: self.kPhone) {
                XCTAssertFalse(self.cache.userExistsWith(login: self.kPhone))
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testUserUpdate() {
        let exp = self.expectation(description: "update user")
        createUser { [unowned self] in
            let kUpdatedName = "test"
            let u = User(login: self.kPhone,
                         name: kUpdatedName,
                         email: self.kMail,
                         password: "")
            self.cache.update(user: u) {
                let u2 = self.cache.getUserWith(login: self.kPhone)
                XCTAssert(u2?.name == kUpdatedName)
                self.cache.deleteUserWith(login: self.kPhone) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testModelCreation() {
        let kID = "123456453333111"
        let model = Model(id: kID,
                          img: kID,
                          path: kID,
                          descr: "")
        let exp = self.expectation(description: "update user")
        createUser { [unowned self] in
            self.cache.create(models: [model]) {
                let itms = self.cache.modelsForCurrentUser()
                XCTAssert(itms.count == 1)
                let i = self.cache.modelWith(id: kID)
                XCTAssert(i != nil)
                XCTAssert(i?.id == kID)
                self.cache.delete(modelIDs: [model]) {
                    XCTAssert(self.cache.modelsForCurrentUser().count == 0)
                    self.cache.deleteUserWith(login: self.kPhone) {
                        exp.fulfill()
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    private func createUser(completion: @escaping (() -> Void)) {
        let u = User(login: kPhone,
                     name: kName,
                     email: kMail,
                     password: "")
        cache.create(user: u) {
            self.secrets.login = self.kPhone
            completion()
        }
    }
}
