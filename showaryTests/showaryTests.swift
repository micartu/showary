//
//  showaryTests.swift
//  showaryTests
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import XCTest
@testable import showary

class showaryTests: XCTestCase {
    func testSecrets() {
        let kv = KeyValueStorage(suiteName: nil)!
        var s: SecretService = Secret(storage: kv)
        let name = "login"
        let old = s.login
        s.login = name
        XCTAssert(s.login == name)
        s.login = old
    }

    func testServicesAssembler() {
        // check services locator
        var s: SecretService = ServicesAssembler.inject()
        let kPass = "new_pass"
        let old = s.password
        s.password = kPass
        XCTAssert(s.password == kPass)
        s.password = old
    }
}
